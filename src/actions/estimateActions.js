import { TITLE_UPDATE, AREA_UPDATE, LENGTH_UPDATE, ASSET_ADD, ASSET_REMOVE } from 'constants/estimateConstants';

export function updateTitle(title) {
  return {
    type: TITLE_UPDATE,
    payload: title
  };
}

export function updateArea(area) {
  return {
    type: AREA_UPDATE,
    payload: area
  };
}
export function updateLength(length) {
  return {
    type: LENGTH_UPDATE,
    payload: length
  };
}
export function addAsset(asset) {
  return {
    type: ASSET_ADD,
    payload: asset
  };
}
export function removeAsset(assetName) {
  return {
    type: ASSET_REMOVE,
    payload: assetName
  };
}