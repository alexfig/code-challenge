import { INITIALIZE_STEPS, STEPS_UPDATE } from 'constants/stepsConstants';

export function initializeSteps(steps) {
  return {
    type: INITIALIZE_STEPS,
    payload: steps
  };
}

export function updateSteps(step) {
  return {
    type: STEPS_UPDATE,
    payload: step
  };
}
