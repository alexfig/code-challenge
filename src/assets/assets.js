export default {
    "report_spec": [
        {
            "asset_id": "38f492ef-ecd0-448a-2c21-947bcd211040",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/821e90df1f6cb2204589",
            "processing_units_km": "3",
            "storefront_clicks": 181,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "Curb Line"
                        },
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -117.20006637468936,
                                    34.06384253257536,
                                    360.82
                                ],
                                [
                                    -117.2000664880753,
                                    34.06386026077752,
                                    360.9000050628662
                                ],
                                [
                                    -117.20006680323436,
                                    34.06387228810934,
                                    360.94000564346317
                                ],
                                [
                                    -117.20006711839358,
                                    34.063884315441136,
                                    360.9922808410645
                                ],
                                [
                                    -117.20006738476526,
                                    34.06390610241868,
                                    361.0786100944519
                                ],
                                [
                                    -117.20006778209007,
                                    34.06391885042493,
                                    361.13571830978395
                                ],
                                [
                                    -117.20006787528493,
                                    34.06393170928964,
                                    361.1999861064911
                                ],
                                [
                                    -117.20006850055404,
                                    34.06395361506587,
                                    361.2887024791718
                                ],
                                [
                                    -117.20006876706668,
                                    34.06397546173446,
                                    361.3899912754059
                                ],
                                [
                                    -117.20006922686566,
                                    34.06398799620868,
                                    361.4400076133728
                                ],
                                [
                                    -117.2000696866647,
                                    34.06400053068287,
                                    361.4931200130463
                                ],
                                [
                                    -117.20007002953056,
                                    34.06402434704769,
                                    361.59
                                ],
                                [
                                    -117.20007005870923,
                                    34.06403676283969,
                                    361.63999348526005
                                ],
                                [
                                    -117.20007008788792,
                                    34.06404917863165,
                                    361.68999070014956
                                ],
                                [
                                    -117.20007074523272,
                                    34.06406947268378,
                                    361.78312943763734
                                ],
                                [
                                    -117.20007084438137,
                                    34.06408114221759,
                                    361.83761407508854
                                ],
                                [
                                    -117.20007105090484,
                                    34.0640927220393,
                                    361.88
                                ],
                                [
                                    -117.2000711156649,
                                    34.06410501839014,
                                    361.93
                                ],
                                [
                                    -117.20007118126661,
                                    34.06411767288882,
                                    361.97999612083436
                                ],
                                [
                                    -117.20007142884239,
                                    34.06413146123043,
                                    362.0323221195221
                                ],
                                [
                                    -117.20007156420372,
                                    34.06414327993401,
                                    362.08
                                ],
                                [
                                    -117.20007163071755,
                                    34.06415632242609,
                                    362.13000420341496
                                ],
                                [
                                    -117.20007183731141,
                                    34.0641679320933,
                                    362.18
                                ],
                                [
                                    -117.20007183161074,
                                    34.06418076578251,
                                    362.23000307693485
                                ],
                                [
                                    -117.2000719300584,
                                    34.064192136859575,
                                    362.27999411659243
                                ],
                                [
                                    -117.20007228479896,
                                    34.06420574595196,
                                    362.33
                                ],
                                [
                                    -117.20007237868741,
                                    34.064215177061556,
                                    362.3700000606537
                                ],
                                [
                                    -117.20007247222526,
                                    34.06422445894284,
                                    362.39999784126286
                                ],
                                [
                                    -117.20007267720624,
                                    34.0642353821599,
                                    362.44
                                ],
                                [
                                    -117.20007277733748,
                                    34.06424746953253,
                                    362.49
                                ],
                                [
                                    -117.20007272336002,
                                    34.06425502059942,
                                    362.52
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Curb line is a continuous solid line that creates a boundary between a roadway and a sidewalk area.Curb lines are extracted in the form of a line string.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/curb_line.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Curb Line",
            "visible": "1",
            "quality_metrics": "97%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a941ec95aa33b377dd1916",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "24bb3c3d-9f60-bdc3-b6d3-d8deef806fe0",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/773ca8c491eca426cdfc",
            "processing_units_km": "2.5",
            "storefront_clicks": 7,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Name": "TrafficLightORSignal"
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                                -117.20029965562789,
                                34.06086511767679,
                                356
                            ]
                        }
                    }
                ]
            },
            "asset_description": "A long, slender, rounded piece of wood or metal, typically used with one end placed in the ground as a support for something. The base or top of the pole in annotated in the form of a point. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/poles_bot.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Pole (bottom) ",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9453595aa33b377dd191a",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "87822924-867a-d649-6b6b-e04fa9624aa8",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/308fe5eb66cd69e814c3",
            "processing_units_km": "2.5",
            "storefront_clicks": 69,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Name": "Light Pole"
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                                -117.20435499800325,
                                34.06725593141679,
                                350.83
                            ]
                        }
                    }
                ]
            },
            "asset_description": "A long, slender, rounded piece of wood or metal with one end placed in the ground as a support for street light.We annotate the bottom of the pole in form of a point. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/light_pole_bot.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Light Pole (bottom)",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a948de95aa33b377dd1921",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "ce9f9de5-1823-db38-c282-1ba37c961bc0",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/7f3b259035c80bc1d7a4",
            "processing_units_km": "2.5",
            "storefront_clicks": 7,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Name": "Light Pole"
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                                -117.20435499800325,
                                34.06725593141679,
                                355.83
                            ]
                        }
                    }
                ]
            },
            "asset_description": "A long, slender, rounded piece of wood or metal with one end placed in the ground as a support for street light.The highest point of the structure is annotated in the form of a point.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/light_pole_top.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Light Pole (top)",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a948f195aa33b377dd1922",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "fe2586ff-017d-f6da-ae14-88811d522aca",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.30231389403343,
                                    37.87160869564001
                                ],
                                [
                                    -122.30212077498436,
                                    37.871006320010814
                                ],
                                [
                                    -122.30195179581642,
                                    37.870490750314694
                                ],
                                [
                                    -122.30177879333496,
                                    37.869955062168124
                                ],
                                [
                                    -122.3015870153904,
                                    37.869346320910275
                                ],
                                [
                                    -122.30146631598473,
                                    37.86897366291062
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.30148509144783,
                                    37.868968369459544
                                ],
                                [
                                    -122.30166614055634,
                                    37.86954323602401
                                ],
                                [
                                    -122.30191022157669,
                                    37.87030230710246
                                ],
                                [
                                    -122.30216100811958,
                                    37.87106560500124
                                ],
                                [
                                    -122.30233132839203,
                                    37.871607636987676
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "The continuous parallel lines created along the highest point of metallic tracks for railwaysThe top of the rail is extracted in the form of linestring. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/top_of_rail.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Top of Rail",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad0829e0a32610bedc076",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "c4adf9f4-54f3-372e-6123-5e38e6c93a49",
            "asset_image_1_url": "",
            "asset_time_complexity": "6",
            "processing_units_km": "1.5",
            "storefront_clicks": 16,
            "geojson_dict": "",
            "asset_description": "Points in the point cloud which are above ground, a significant distance away from the other points (5 sigma)We classify high point noise in the form of classified LAS. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "geojson_iframe": "",
            "asset_category": "point_cloud_classification",
            "asset_name": "High Point Noise (16)",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55d264ea6c5e94d21f6c2282",
            "output_format": "classified_las_asprs",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "ec6c67cc-b420-4692-a72f-c6711fd9a785",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 38,
            "geojson_dict": "",
            "asset_description": "A structure with a roof and walls, such as a house, school, store, or factory. The output is in the form of classified LAS.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Building (6)",
            "visible": "1",
            "quality_metrics": "95%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e143a8c00930325db2f",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "09b0e486-8167-ec00-a026-8cbe14beb8b2",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 12,
            "geojson_dict": "",
            "asset_description": "Road classification is all the points on the surface of a road. The points are extracted in the form of classified LAS.  ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Road (11)",
            "visible": "1",
            "quality_metrics": "95%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e3e3a8c00930325db32",
            "output_format": "classified_las_asprs",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "f1b8fe1c-6e83-db15-fa4c-182ac35262ee",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/3e65819c18aa0a3b89fc",
            "processing_units_km": "3",
            "storefront_clicks": 17,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "Traffic Barrier"
                        },
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -117.20030694062045,
                                    34.064928709761276,
                                    364.68
                                ],
                                [
                                    -117.2003057748853,
                                    34.06493989497829,
                                    364.66
                                ],
                                [
                                    -117.20030584703653,
                                    34.064970558794876,
                                    364.72
                                ],
                                [
                                    -117.20030669919858,
                                    34.06501033033227,
                                    364.82
                                ],
                                [
                                    -117.20030738194828,
                                    34.06507021407843,
                                    364.95
                                ],
                                [
                                    -117.20030816733777,
                                    34.06512766257967,
                                    365.06
                                ],
                                [
                                    -117.2003090833795,
                                    34.065194580611454,
                                    365.24
                                ],
                                [
                                    -117.20030988341462,
                                    34.06525825206223,
                                    365.31
                                ],
                                [
                                    -117.20031027621735,
                                    34.06528702140587,
                                    365.35
                                ],
                                [
                                    -117.20031107476908,
                                    34.065350061541835,
                                    365.49
                                ],
                                [
                                    -117.20031196280338,
                                    34.065405074796196,
                                    365.62
                                ],
                                [
                                    -117.20031211340384,
                                    34.0654230219697,
                                    365.68
                                ],
                                [
                                    -117.20031254992584,
                                    34.06547036997637,
                                    365.77
                                ],
                                [
                                    -117.2003129671363,
                                    34.065509510903105,
                                    365.83
                                ],
                                [
                                    -117.20031377184742,
                                    34.06557516647966,
                                    365.92
                                ],
                                [
                                    -117.20031432162169,
                                    34.0656245886253,
                                    366.01
                                ],
                                [
                                    -117.20031525613926,
                                    34.065699352980175,
                                    366.17
                                ],
                                [
                                    -117.20031606149135,
                                    34.065765279117734,
                                    366.25
                                ],
                                [
                                    -117.20031659450278,
                                    34.065807576434835,
                                    366.31
                                ],
                                [
                                    -117.20031703760944,
                                    34.065857720256666,
                                    366.38
                                ],
                                [
                                    -117.20031793096032,
                                    34.065914988198436,
                                    366.46
                                ],
                                [
                                    -117.20031872422024,
                                    34.06597577363585,
                                    366.53
                                ],
                                [
                                    -117.20031920863364,
                                    34.06599741797271,
                                    366.55
                                ],
                                [
                                    -117.20032011782372,
                                    34.066015363908086,
                                    366.57
                                ],
                                [
                                    -117.20031947036584,
                                    34.06601653740768,
                                    366.57
                                ],
                                [
                                    -117.20031903985914,
                                    34.066017800741704,
                                    366.52
                                ],
                                [
                                    -117.20031938597863,
                                    34.066026728792146,
                                    366.56
                                ],
                                [
                                    -117.20032008543375,
                                    34.06604765127413,
                                    366.56
                                ],
                                [
                                    -117.20032045914388,
                                    34.06606830372287,
                                    366.58
                                ],
                                [
                                    -117.20032030649942,
                                    34.066095540755256,
                                    366.6
                                ],
                                [
                                    -117.20032043143586,
                                    34.066140040656656,
                                    366.6340145888595
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Traffic Barriers are ideal where you need to separate people or vehicles from potentially dangerous situations.The traffic barriers are extracted in the form of a line string. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/traffic_barrier.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Traffic Barrier",
            "visible": "1",
            "quality_metrics": "92%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a945ba95aa33b377dd191d",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "db23c9e6-e11d-e001-c45c-c528dd35c103",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/3c395b90babf3e9d53ba",
            "processing_units_km": "3",
            "storefront_clicks": 28,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29344919323921,
                                    37.84404894088849
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29713186621666,
                                    37.85548048009374
                                ],
                                [
                                    -122.29726865887642,
                                    37.85590085585191
                                ],
                                [
                                    -122.29749798774719,
                                    37.85661453615822
                                ],
                                [
                                    -122.2977165877819,
                                    37.857293267249865
                                ],
                                [
                                    -122.29799821972847,
                                    37.85817423182701
                                ],
                                [
                                    -122.29822620749474,
                                    37.85887941944204
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29827046394348,
                                    37.858869889924634
                                ],
                                [
                                    -122.29806929826736,
                                    37.85825152749542
                                ],
                                [
                                    -122.29783460497856,
                                    37.85751562709084
                                ],
                                [
                                    -122.29757577180862,
                                    37.856708775326155
                                ],
                                [
                                    -122.29732632637024,
                                    37.855935798817086
                                ],
                                [
                                    -122.29717746376991,
                                    37.855468832368565
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29831203818321,
                                    37.85885506622847
                                ],
                                [
                                    -122.2981108725071,
                                    37.858231409452536
                                ],
                                [
                                    -122.29788824915886,
                                    37.85754633387331
                                ],
                                [
                                    -122.29766428470612,
                                    37.85684219236912
                                ],
                                [
                                    -122.29742020368576,
                                    37.856084041515494
                                ],
                                [
                                    -122.29721769690514,
                                    37.85545612575718
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Track centerline is an imaginary line running exactly between the two rails.  The height of this centerline is set at the lower of the top of both rails.The centerline is extracted in the form of a line string. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/track_centerline.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Track Centerline",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad03b9e0a32610bedc075",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "1a1ed44c-d387-a418-ddc6-dccc773dd8a4",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 12,
            "geojson_dict": "",
            "asset_description": "Vegetation is assemblages of plant species and the ground cover they provide.The output is delivered in classified LAS.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Vegetation (L/M/H-5)",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e013a8c00930325db2e",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "f625747d-8a5d-b9fc-4674-976fe384b4c8",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2",
            "storefront_clicks": 81,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.18227718258277,
                                        37.43460714449312
                                    ],
                                    [
                                        -122.18209076905623,
                                        37.43441759190425
                                    ],
                                    [
                                        -122.18205321813002,
                                        37.434437825068656
                                    ],
                                    [
                                        -122.1822463371791,
                                        37.43463057230784
                                    ],
                                    [
                                        -122.18227718258277,
                                        37.43460714449312
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.18224901938811,
                                        37.43468062261474
                                    ],
                                    [
                                        -122.18223829055205,
                                        37.43464122131212
                                    ],
                                    [
                                        -122.18204248929396,
                                        37.43478711251806
                                    ],
                                    [
                                        -122.18209881568328,
                                        37.434793501907436
                                    ],
                                    [
                                        -122.18224901938811,
                                        37.43468062261474
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.18200359726325,
                                        37.43480202109244
                                    ],
                                    [
                                        -122.18203578377143,
                                        37.434779658229765
                                    ],
                                    [
                                        -122.18180913710967,
                                        37.43461353389787
                                    ],
                                    [
                                        -122.18180779600516,
                                        37.43465506501539
                                    ],
                                    [
                                        -122.18200359726325,
                                        37.43480202109244
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.18181181931868,
                                        37.43460927429477
                                    ],
                                    [
                                        -122.18178902054206,
                                        37.43458691137449
                                    ],
                                    [
                                        -122.18200762057677,
                                        37.434421851518266
                                    ],
                                    [
                                        -122.1820491948165,
                                        37.43444314958469
                                    ],
                                    [
                                        -122.18181181931868,
                                        37.43460927429477
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A marked part of a road where pedestrians have right of way to cross.The crossing is extracted in form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/pedestrian_crossing.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Pedestrian Crossing",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9426095aa33b377dd1919",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "3e1c3cfc-9252-7bcc-c6c0-d10155f19df3",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/6e610fd182f5b7d29488",
            "processing_units_km": "2",
            "storefront_clicks": 13,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1819116268307,
                                    37.43473493248436
                                ],
                                [
                                    -122.18007699586451,
                                    37.436085212258796
                                ],
                                [
                                    -122.1819116268307,
                                    37.43473493248436
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1822173986584,
                                    37.434534731201225
                                ],
                                [
                                    -122.18422905541956,
                                    37.43305663281589
                                ],
                                [
                                    -122.1822173986584,
                                    37.434534731201225
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1819116268307,
                                    37.434483615894166
                                ],
                                [
                                    -122.1817131433636,
                                    37.43433026976361
                                ],
                                [
                                    -122.1814985666424,
                                    37.43424933695691
                                ],
                                [
                                    -122.18136407900602,
                                    37.43419609164162
                                ],
                                [
                                    -122.18127288389951,
                                    37.43415562517668
                                ],
                                [
                                    -122.1810368495062,
                                    37.434102379794716
                                ],
                                [
                                    -122.18052224256098,
                                    37.43398098018222
                                ],
                                [
                                    -122.18038276769221,
                                    37.4339532925237
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18149320222437,
                                    37.43424933695691
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18256015563384,
                                    37.435144917483406
                                ],
                                [
                                    -122.18252931023017,
                                    37.43507889407681
                                ],
                                [
                                    -122.18249310040846,
                                    37.43503203872069
                                ],
                                [
                                    -122.18241129303351,
                                    37.43496175563151
                                ],
                                [
                                    -122.18216050649062,
                                    37.43474771127149
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18422983074561,
                                    37.43305663281589
                                ],
                                [
                                    -122.18488697195426,
                                    37.43256889654032
                                ],
                                [
                                    -122.18525365926325,
                                    37.43226858572431
                                ],
                                [
                                    -122.18651421368122,
                                    37.43131865847243
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18651966191828,
                                    37.43131865847243
                                ],
                                [
                                    -122.18935743905604,
                                    37.42926115138961
                                ],
                                [
                                    -122.19011918641627,
                                    37.428541223547775
                                ],
                                [
                                    -122.19125099480152,
                                    37.42675202378011
                                ],
                                [
                                    -122.19147093594074,
                                    37.42647511999517
                                ],
                                [
                                    -122.19152466394007,
                                    37.426428259253335
                                ],
                                [
                                    -122.19186262227595,
                                    37.426162004481505
                                ],
                                [
                                    -122.19223813153803,
                                    37.42593834974143
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1921791229397,
                                    37.425968170412034
                                ],
                                [
                                    -122.19518856145442,
                                    37.423897735637745
                                ],
                                [
                                    -122.19582156278193,
                                    37.42348023362338
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Road centerlines are line segments representing centerlines of all roadways or carriageways within the jurisdiction of a local or federal government.The centerlines are extracted in the form of line strings. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/centerline.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Road Centerline",
            "visible": "1",
            "quality_metrics": "98%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a941b295aa33b377dd1915",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "a0a0e665-b21c-1ebc-a333-4a52806dabb4",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/3196e531c110144b876e",
            "processing_units_km": "3.5",
            "storefront_clicks": 13,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1819116268307,
                                    37.43473493248436
                                ],
                                [
                                    -122.18007699586451,
                                    37.436085212258796
                                ],
                                [
                                    -122.1819116268307,
                                    37.43473493248436
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1822173986584,
                                    37.434534731201225
                                ],
                                [
                                    -122.18422905541956,
                                    37.43305663281589
                                ],
                                [
                                    -122.1822173986584,
                                    37.434534731201225
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1819116268307,
                                    37.434483615894166
                                ],
                                [
                                    -122.1817131433636,
                                    37.43433026976361
                                ],
                                [
                                    -122.1814985666424,
                                    37.43424933695691
                                ],
                                [
                                    -122.18136407900602,
                                    37.43419609164162
                                ],
                                [
                                    -122.18127288389951,
                                    37.43415562517668
                                ],
                                [
                                    -122.1810368495062,
                                    37.434102379794716
                                ],
                                [
                                    -122.18052224256098,
                                    37.43398098018222
                                ],
                                [
                                    -122.18038276769221,
                                    37.4339532925237
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18149320222437,
                                    37.43424933695691
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18256015563384,
                                    37.435144917483406
                                ],
                                [
                                    -122.18252931023017,
                                    37.43507889407681
                                ],
                                [
                                    -122.18249310040846,
                                    37.43503203872069
                                ],
                                [
                                    -122.18241129303351,
                                    37.43496175563151
                                ],
                                [
                                    -122.18216050649062,
                                    37.43474771127149
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18422983074561,
                                    37.43305663281589
                                ],
                                [
                                    -122.18488697195426,
                                    37.43256889654032
                                ],
                                [
                                    -122.18525365926325,
                                    37.43226858572431
                                ],
                                [
                                    -122.18651421368122,
                                    37.43131865847243
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.18651966191828,
                                    37.43131865847243
                                ],
                                [
                                    -122.18935743905604,
                                    37.42926115138961
                                ],
                                [
                                    -122.19011918641627,
                                    37.428541223547775
                                ],
                                [
                                    -122.19125099480152,
                                    37.42675202378011
                                ],
                                [
                                    -122.19147093594074,
                                    37.42647511999517
                                ],
                                [
                                    -122.19152466394007,
                                    37.426428259253335
                                ],
                                [
                                    -122.19186262227595,
                                    37.426162004481505
                                ],
                                [
                                    -122.19223813153803,
                                    37.42593834974143
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.1921791229397,
                                    37.425968170412034
                                ],
                                [
                                    -122.19518856145442,
                                    37.423897735637745
                                ],
                                [
                                    -122.19582156278193,
                                    37.42348023362338
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Lane markings are used to convey messages to roadway users. They indicate which part of the road to use, provide information about conditions ahead, and indicate where passing is allowed. Lane markings are extracted in the form of line strings. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/lane_marking.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Lane Marking",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "14",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9422395aa33b377dd1917",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "017741d1-c410-e286-278e-54a7aa710bfb",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/a0c5fe72c8c199a48a90",
            "processing_units_km": "2.5",
            "storefront_clicks": 72,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Name": "TrafficLightORSignal"
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                                -117.20029965562789,
                                34.06086511767679,
                                359
                            ]
                        }
                    }
                ]
            },
            "asset_description": "A long, slender, rounded piece of wood or metal, typically used with one end placed in the ground as a support for something. The top of the pole is annotated in the form of a point. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/poles_top.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Pole (top)",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9454a95aa33b377dd191b",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "c930cf0a-d118-e098-9d40-921f006e5fdc",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 116,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46947050094604,
                                        37.806024609144636
                                    ],
                                    [
                                        -122.46893405914308,
                                        37.80612209172951
                                    ],
                                    [
                                        -122.46883749961853,
                                        37.80583388197601
                                    ],
                                    [
                                        -122.46936857700346,
                                        37.80572368383182
                                    ],
                                    [
                                        -122.46947050094604,
                                        37.806024609144636
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.4690306186676,
                                        37.80574063740317
                                    ],
                                    [
                                        -122.4689769744873,
                                        37.80558381671973
                                    ],
                                    [
                                        -122.4687784910202,
                                        37.805626200721065
                                    ],
                                    [
                                        -122.46882677078247,
                                        37.80577878292446
                                    ],
                                    [
                                        -122.4690306186676,
                                        37.80574063740317
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46930420398712,
                                        37.805507525456015
                                    ],
                                    [
                                        -122.46908962726592,
                                        37.80555414790431
                                    ],
                                    [
                                        -122.46895551681519,
                                        37.80511759109957
                                    ],
                                    [
                                        -122.46917009353636,
                                        37.80506672994475
                                    ],
                                    [
                                        -122.46930420398712,
                                        37.805507525456015
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46890187263489,
                                        37.805261697514624
                                    ],
                                    [
                                        -122.46868193149565,
                                        37.80530832011811
                                    ],
                                    [
                                        -122.46862828731537,
                                        37.80518116749381
                                    ],
                                    [
                                        -122.46883749961853,
                                        37.8051303063828
                                    ],
                                    [
                                        -122.46890187263489,
                                        37.805261697514624
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46913254261015,
                                        37.80496500753008
                                    ],
                                    [
                                        -122.46899843215942,
                                        37.80466831635362
                                    ],
                                    [
                                        -122.46849954128265,
                                        37.804825138981116
                                    ],
                                    [
                                        -122.4686336517334,
                                        37.805100637385195
                                    ],
                                    [
                                        -122.46913254261015,
                                        37.80496500753008
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46811866760252,
                                        37.80384605172122
                                    ],
                                    [
                                        -122.46793627738953,
                                        37.80396049116176
                                    ],
                                    [
                                        -122.46764123439787,
                                        37.80371465807083
                                    ],
                                    [
                                        -122.4678236246109,
                                        37.80359597973408
                                    ],
                                    [
                                        -122.46811866760252,
                                        37.80384605172122
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46722817420958,
                                        37.80541851888209
                                    ],
                                    [
                                        -122.46706187725067,
                                        37.80530832011811
                                    ],
                                    [
                                        -122.4669224023819,
                                        37.80543971093323
                                    ],
                                    [
                                        -122.46709942817688,
                                        37.80553719429016
                                    ],
                                    [
                                        -122.46722817420958,
                                        37.80541851888209
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46679365634917,
                                        37.80554143269405
                                    ],
                                    [
                                        -122.46687948703767,
                                        37.80546090297828
                                    ],
                                    [
                                        -122.46670246124266,
                                        37.80537613476155
                                    ],
                                    [
                                        -122.46662199497221,
                                        37.80545242616099
                                    ],
                                    [
                                        -122.46679365634917,
                                        37.80554143269405
                                    ]
                                ]
                            ]
                        }
                    },
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.46752858161926,
                                        37.80560500872344
                                    ],
                                    [
                                        -122.46741056442261,
                                        37.80554143269405
                                    ],
                                    [
                                        -122.46731936931609,
                                        37.8056431543148
                                    ],
                                    [
                                        -122.4674427509308,
                                        37.80568553828206
                                    ],
                                    [
                                        -122.46752858161926,
                                        37.80560500872344
                                    ]
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Building roof corners is the outline of the roof perimeter of the building.The roof corners are extracted in the form of a polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/building_roof_outline.png",
            "classification_type": "polygon_geojson",
            "asset_category": "buildings",
            "asset_name": "Building Roof Corners",
            "visible": "1",
            "quality_metrics": "97%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97a3d85de584c02741c6e",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "37a25be3-9cd8-8e17-8308-a487c00e8747",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 17,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29221805930138,
                                        37.83975472372039
                                    ],
                                    [
                                        -122.29221001267433,
                                        37.83980115573949
                                    ],
                                    [
                                        -122.29221940040588,
                                        37.83980115573949
                                    ],
                                    [
                                        -122.29221940040588,
                                        37.83981463599762
                                    ],
                                    [
                                        -122.29219660162926,
                                        37.839817631610195
                                    ],
                                    [
                                        -122.29219526052475,
                                        37.839796662319564
                                    ],
                                    [
                                        -122.29221001267433,
                                        37.83978767547888
                                    ],
                                    [
                                        -122.29220733046532,
                                        37.839756221527935
                                    ],
                                    [
                                        -122.29221805930138,
                                        37.83975472372039
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A signal is a mechanical or electrical device erected beside a railway line to pass information relating to the state of the line ahead to train/engine drivers (engineers in the US). The outline of the railway signal is extracted in the form of a polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/rail_signal_outline.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Rail Signal Outline",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad17d9e0a32610bedc07a",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "7c8bc85c-61cb-be59-3b25-bc448d7d2d81",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2",
            "storefront_clicks": 9,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104056954384,
                                        37.83162118039919
                                    ],
                                    [
                                        -122.29100972414017,
                                        37.831618184453745
                                    ],
                                    [
                                        -122.29100972414017,
                                        37.83160620067079
                                    ],
                                    [
                                        -122.29104056954384,
                                        37.831604702697774
                                    ],
                                    [
                                        -122.29104056954384,
                                        37.83162118039919
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104191064835,
                                        37.83159122093934
                                    ],
                                    [
                                        -122.29100704193115,
                                        37.83159122093934
                                    ],
                                    [
                                        -122.29100570082664,
                                        37.831579237152
                                    ],
                                    [
                                        -122.29104325175285,
                                        37.831579237152
                                    ],
                                    [
                                        -122.29104191064835,
                                        37.83159122093934
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104325175285,
                                        37.831568751336455
                                    ],
                                    [
                                        -122.29100435972214,
                                        37.831568751336455
                                    ],
                                    [
                                        -122.29100435972214,
                                        37.831556767545486
                                    ],
                                    [
                                        -122.29104459285736,
                                        37.831556767545486
                                    ],
                                    [
                                        -122.29104325175285,
                                        37.831568751336455
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104593396187,
                                        37.83154028982967
                                    ],
                                    [
                                        -122.29100435972214,
                                        37.83154028982967
                                    ],
                                    [
                                        -122.29100435972214,
                                        37.8315298040086
                                    ],
                                    [
                                        -122.29104861617088,
                                        37.8315298040086
                                    ],
                                    [
                                        -122.29104593396187,
                                        37.83154028982967
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104995727539,
                                        37.83151931818606
                                    ],
                                    [
                                        -122.29100167751312,
                                        37.83151782021132
                                    ],
                                    [
                                        -122.29100167751312,
                                        37.83150733438705
                                    ],
                                    [
                                        -122.29104995727539,
                                        37.83150733438705
                                    ],
                                    [
                                        -122.29104995727539,
                                        37.83151931818606
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104995727539,
                                        37.83149385261082
                                    ],
                                    [
                                        -122.29100033640862,
                                        37.8314968485613
                                    ],
                                    [
                                        -122.29104995727539,
                                        37.83149385261082
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29104995727539,
                                        37.831480370832104
                                    ],
                                    [
                                        -122.29099497199059,
                                        37.83147887285654
                                    ],
                                    [
                                        -122.29099631309509,
                                        37.83149385261082
                                    ],
                                    [
                                        -122.29104995727539,
                                        37.83149235463552
                                    ],
                                    [
                                        -122.29104995727539,
                                        37.831480370832104
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A railway tie/railway tie/crosstie is a rectangular support for the rails in railway tracks. Generally laid perpendicular to the rails, ties transfer loads to the track ballast. Ties are extracted in the form of a polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/railroad_ties.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Rail Ties",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad21b9e0a32610bedc07d",
            "output_format": "geojson",
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "5f1e20b7-32cd-eb16-156d-4383da2facc4",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4.5",
            "storefront_clicks": 10,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29091718792915,
                                    37.8314713829783
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29089438915253,
                                    37.83147288095401
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A rail switch is a mechanical installation enabling railway trains to be guided from one track to another, such as at a railway junction or where a spur or siding branches off.  The output is annotated as two points of contact on the set of rails.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/switch.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Rail Switch",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "18",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad1109e0a32610bedc078",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "ba316ac0-91b9-50dc-0e9f-d891b853213e",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4",
            "storefront_clicks": 16,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30007961392403,
                                        37.864397861896066
                                    ],
                                    [
                                        -122.30006217956543,
                                        37.86440103816376
                                    ],
                                    [
                                        -122.29984894394875,
                                        37.86373719524072
                                    ],
                                    [
                                        -122.29987174272537,
                                        37.86373190141347
                                    ],
                                    [
                                        -122.30007961392403,
                                        37.864397861896066
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30004072189331,
                                        37.86440739069871
                                    ],
                                    [
                                        -122.30001926422119,
                                        37.864410566966
                                    ],
                                    [
                                        -122.2998046875,
                                        37.863742489067604
                                    ],
                                    [
                                        -122.2998234629631,
                                        37.86374037153689
                                    ],
                                    [
                                        -122.30004072189331,
                                        37.86440739069871
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29999378323555,
                                        37.86441480198883
                                    ],
                                    [
                                        -122.29997500777245,
                                        37.864417978255794
                                    ],
                                    [
                                        -122.29976043105125,
                                        37.86375307672019
                                    ],
                                    [
                                        -122.29977920651436,
                                        37.8637509591898
                                    ],
                                    [
                                        -122.29999378323555,
                                        37.86441480198883
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Track ballast forms the trackbed upon which railway sleepers (UK) or rail ties (US) are laid. It is packed between, below, and around the ties. The ballast profile is extracted in the form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/ballast_profile.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Ballast Profile",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "16",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aacf3f9e0a32610bedc074",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "21f90710-da7d-15e3-086a-0573cb5378f0",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4",
            "storefront_clicks": 35,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30051144957542,
                                        37.864349114879886
                                    ],
                                    [
                                        -122.30051413178444,
                                        37.86438355295517
                                    ],
                                    [
                                        -122.30047389864922,
                                        37.86439103949113
                                    ],
                                    [
                                        -122.30047389864922,
                                        37.86440900717438
                                    ],
                                    [
                                        -122.30021104216576,
                                        37.86446141289217
                                    ],
                                    [
                                        -122.30021104216576,
                                        37.86440152064022
                                    ],
                                    [
                                        -122.30051144957542,
                                        37.864349114879886
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30024860480898,
                                        37.864515383709445
                                    ],
                                    [
                                        -122.30030739966264,
                                        37.86451855997208
                                    ],
                                    [
                                        -122.30026946749899,
                                        37.86441268447743
                                    ],
                                    [
                                        -122.30020877603715,
                                        37.86441374323314
                                    ],
                                    [
                                        -122.30024860480898,
                                        37.864515383709445
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30054631829262,
                                        37.864456920974945
                                    ],
                                    [
                                        -122.30054631829262,
                                        37.86442847215964
                                    ],
                                    [
                                        -122.30049803853035,
                                        37.86442547754685
                                    ],
                                    [
                                        -122.30024993419647,
                                        37.86447039672578
                                    ],
                                    [
                                        -122.30025127530098,
                                        37.864525797008724
                                    ],
                                    [
                                        -122.30054631829262,
                                        37.864456920974945
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30050844013005,
                                        37.86445926971382
                                    ],
                                    [
                                        -122.30054068246909,
                                        37.864458210958794
                                    ],
                                    [
                                        -122.30050464691368,
                                        37.8643470415944
                                    ],
                                    [
                                        -122.3004743011827,
                                        37.86434810035103
                                    ],
                                    [
                                        -122.30050844013005,
                                        37.86445926971382
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29990152551147,
                                        37.86456302763445
                                    ],
                                    [
                                        -122.29987117978055,
                                        37.864564086388
                                    ],
                                    [
                                        -122.29983135100872,
                                        37.864515383709445
                                    ],
                                    [
                                        -122.29967013931315,
                                        37.86399235726315
                                    ],
                                    [
                                        -122.29964548340683,
                                        37.86399235726315
                                    ],
                                    [
                                        -122.29957341229579,
                                        37.86376260560621
                                    ],
                                    [
                                        -122.2996322071495,
                                        37.86376154684116
                                    ],
                                    [
                                        -122.29990152551147,
                                        37.86456302763445
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30037733912468,
                                        37.864864187022015
                                    ],
                                    [
                                        -122.30037733912468,
                                        37.86483873296003
                                    ],
                                    [
                                        -122.30108678340912,
                                        37.864584191856444
                                    ],
                                    [
                                        -122.3010841012001,
                                        37.86463959205385
                                    ],
                                    [
                                        -122.30037733912468,
                                        37.864864187022015
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "The outline of the side planes of the building (facade).The facade outline is extracted in the form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/building_facade_outline.png",
            "classification_type": "polygon_geojson",
            "asset_category": "buildings",
            "asset_name": "Building Facade Outline",
            "visible": "1",
            "quality_metrics": "95%",
            "processing_units_sqkm": "16",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97a9685de584c02741c71",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "f2772273-16dc-9ca4-57e3-75593604e3a3",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3.5",
            "storefront_clicks": 87,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27787494659424,
                                    37.87454956801756
                                ],
                                [
                                    -122.27778777480125,
                                    37.87385041867465
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27776899933815,
                                    37.873856407134014
                                ],
                                [
                                    -122.2778494656086,
                                    37.87455106511821
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27777972817421,
                                    37.87385041867465
                                ],
                                [
                                    -122.27786287665367,
                                    37.87455106511821
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2778494656086,
                                    37.87455106511821
                                ],
                                [
                                    -122.27646142244339,
                                    37.87473221407608
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2764641046524,
                                    37.87474269375422
                                ],
                                [
                                    -122.27786287665367,
                                    37.87456453902299
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27786421775818,
                                    37.87458549842548
                                ],
                                [
                                    -122.27646678686142,
                                    37.87475616762394
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27560713887215,
                                    37.874842999169644
                                ],
                                [
                                    -122.27646142244339,
                                    37.874729219882035
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2764641046524,
                                    37.87474269375422
                                ],
                                [
                                    -122.27560177445412,
                                    37.874862461398585
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2755990922451,
                                    37.87485497592653
                                ],
                                [
                                    -122.27645874023438,
                                    37.87473520827
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27560579776764,
                                    37.87484000498013
                                ],
                                [
                                    -122.27547839283943,
                                    37.87396569643213
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27546766400337,
                                    37.87396869065719
                                ],
                                [
                                    -122.2755990922451,
                                    37.87485946720988
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27545827627182,
                                    37.873967193544665
                                ],
                                [
                                    -122.27559238672256,
                                    37.874856473021
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Power distribution wires are set of wires that connect power distribution towers. They typically carry resitors and conductors on a distribution line.The wires are extracted in the form of line strings. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/distribution_wire.png",
            "classification_type": "linestring_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Distribution Wire",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "14",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9739595aa33b377dd1927",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "c7209bd2-f1e0-1712-25f8-dcf437627635",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27785617113113,
                                    37.87455106511821
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27777436375618,
                                    37.87385790424879
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27646678686142,
                                    37.87474419085098
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27598264813423,
                                    37.874817548554766
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2755990922451,
                                    37.8748385078853
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27771535515785,
                                    37.873652799242144
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27737471461296,
                                    37.87370220414998
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A distribution pole is a column or post used to support power distribution network. The highest point of the pole above the ground is annotated as a point.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/distribution_pole_top.png",
            "classification_type": "points_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Distribution Pole (top)",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a973c695aa33b377dd1929",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "d12a290a-1140-b328-d777-a0a40ea2fd74",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 10,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27778509259224,
                                    37.87377256865849
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27687180042267,
                                    37.87367675314099
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27547571063042,
                                    37.87387437250925
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27560445666313,
                                    37.87474718504439
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27598801255226,
                                    37.874723231493626
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27646678686142,
                                    37.87466334758259
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27786421775818,
                                    37.87445824481844
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A distribution pole is a column or post used to support power distribution network. The lowest point of the pole above the ground is annotated as a point.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/distribution_pole_bot.png",
            "classification_type": "points_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Distribution Pole (bottom)",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a973ae95aa33b377dd1928",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "b659f15e-ee60-920d-068e-f2983c40609e",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 9,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.2757077217102,
                                        37.87566939082703
                                    ],
                                    [
                                        -122.27570503950119,
                                        37.87565891128071
                                    ],
                                    [
                                        -122.27569565176964,
                                        37.87566190543695
                                    ],
                                    [
                                        -122.27569699287415,
                                        37.875675379138535
                                    ],
                                    [
                                        -122.2757077217102,
                                        37.87566939082703
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.27585792541504,
                                        37.87581460724394
                                    ],
                                    [
                                        -122.27585524320602,
                                        37.87580412771828
                                    ],
                                    [
                                        -122.27584451436996,
                                        37.87580861894375
                                    ],
                                    [
                                        -122.275849878788,
                                        37.87582508676812
                                    ],
                                    [
                                        -122.27585792541504,
                                        37.87581460724394
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A sign giving information or instructions to road users.The outline of the sign is extracted in form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/road_sign_outline.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Road Sign Outline",
            "visible": "1",
            "quality_metrics": "92%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9459d95aa33b377dd191c",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "8c60dd86-72e1-3ae2-2bea-294b82198362",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 45,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.27289274334908,
                                        37.87174993651934
                                    ],
                                    [
                                        -122.27289274334908,
                                        37.87172897631014
                                    ],
                                    [
                                        -122.27288067340851,
                                        37.87172897631014
                                    ],
                                    [
                                        -122.27288201451302,
                                        37.871748439361745
                                    ],
                                    [
                                        -122.27289274334908,
                                        37.87174993651934
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.27317169308662,
                                        37.87150889375303
                                    ],
                                    [
                                        -122.27316901087761,
                                        37.8714879334752
                                    ],
                                    [
                                        -122.27315828204155,
                                        37.871489430638114
                                    ],
                                    [
                                        -122.27315828204155,
                                        37.87150889375303
                                    ],
                                    [
                                        -122.27317169308662,
                                        37.87150889375303
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Traffic lights are installed to regulate traffic flow and make driving safe.The outline of the traffic light is extracted in the form of a polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/traffic_signal.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Traffic Signal Outline",
            "visible": "1",
            "quality_metrics": "92%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a945d395aa33b377dd191e",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "7a125dfc-1ba6-2471-ccc6-289887228f56",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "1.5",
            "storefront_clicks": 26,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.27289274334908,
                                        37.87174993651934
                                    ],
                                    [
                                        -122.27289274334908,
                                        37.87172897631014
                                    ],
                                    [
                                        -122.27288067340851,
                                        37.87172897631014
                                    ],
                                    [
                                        -122.27288201451302,
                                        37.871748439361745
                                    ],
                                    [
                                        -122.27289274334908,
                                        37.87174993651934
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.27317169308662,
                                        37.87150889375303
                                    ],
                                    [
                                        -122.27316901087761,
                                        37.8714879334752
                                    ],
                                    [
                                        -122.27315828204155,
                                        37.871489430638114
                                    ],
                                    [
                                        -122.27315828204155,
                                        37.87150889375303
                                    ],
                                    [
                                        -122.27317169308662,
                                        37.87150889375303
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -121.60423278808594,
                                    38.73855285385149
                                ],
                                [
                                    -121.60354614257812,
                                    38.72034047123783
                                ],
                                [
                                    -121.61109924316406,
                                    38.70962513631028
                                ],
                                [
                                    -121.63307189941406,
                                    38.69086943518648
                                ],
                                [
                                    -121.629638671875,
                                    38.676933444637925
                                ],
                                [
                                    -121.61041259765625,
                                    38.66138625351511
                                ],
                                [
                                    -121.59530639648438,
                                    38.64476310916202
                                ],
                                [
                                    -121.58912658691406,
                                    38.64208159560713
                                ],
                                [
                                    -121.57333374023438,
                                    38.64851705956594
                                ],
                                [
                                    -121.56440734863281,
                                    38.642617906345265
                                ],
                                [
                                    -121.56166076660156,
                                    38.62116234642251
                                ],
                                [
                                    -121.56097412109375,
                                    38.61043215866372
                                ],
                                [
                                    -121.5472412109375,
                                    38.59648051509767
                                ],
                                [
                                    -121.53488159179688,
                                    38.60292007223949
                                ],
                                [
                                    -121.52320861816406,
                                    38.603993275591684
                                ],
                                [
                                    -121.51290893554688,
                                    38.60077361738762
                                ],
                                [
                                    -121.5087890625,
                                    38.5959438592568
                                ],
                                [
                                    -121.50672912597656,
                                    38.58843025622397
                                ],
                                [
                                    -121.50672912597656,
                                    38.58091586687017
                                ],
                                [
                                    -121.51153564453125,
                                    38.574474335898074
                                ],
                                [
                                    -121.519775390625,
                                    38.56910595216649
                                ],
                                [
                                    -121.52183532714844,
                                    38.5626633622084
                                ],
                                [
                                    -121.51771545410156,
                                    38.55514627725395
                                ],
                                [
                                    -121.51153564453125,
                                    38.54494326173158
                                ],
                                [
                                    -121.51565551757812,
                                    38.537424323873275
                                ],
                                [
                                    -121.5252685546875,
                                    38.533127435052776
                                ],
                                [
                                    -121.53144836425781,
                                    38.52668162061619
                                ],
                                [
                                    -121.52732849121094,
                                    38.521846880854994
                                ],
                                [
                                    -121.53076171875,
                                    38.517549061739984
                                ],
                                [
                                    -121.54586791992188,
                                    38.515937313413474
                                ],
                                [
                                    -121.55410766601562,
                                    38.51110185192187
                                ],
                                [
                                    -121.55891418457031,
                                    38.50250467407243
                                ],
                                [
                                    -121.55891418457031,
                                    38.493369048060764
                                ],
                                [
                                    -121.55204772949219,
                                    38.48261976950729
                                ],
                                [
                                    -121.54312133789062,
                                    38.477244528955595
                                ],
                                [
                                    -121.53076171875,
                                    38.47401919222663
                                ],
                                [
                                    -121.5142822265625,
                                    38.47240646975171
                                ],
                                [
                                    -121.50535583496094,
                                    38.468105700129684
                                ],
                                [
                                    -121.50398254394531,
                                    38.459503391364166
                                ],
                                [
                                    -121.50123596191406,
                                    38.44874906264676
                                ],
                                [
                                    -121.50260925292969,
                                    38.43960662292255
                                ],
                                [
                                    -121.51290893554688,
                                    38.43422817624596
                                ],
                                [
                                    -121.52664184570312,
                                    38.43422817624596
                                ],
                                [
                                    -121.53350830078125,
                                    38.42831142210913
                                ],
                                [
                                    -121.52664184570312,
                                    38.42185622831224
                                ],
                                [
                                    -121.51634216308594,
                                    38.40625379485267
                                ],
                                [
                                    -121.51222229003906,
                                    38.39333888832238
                                ],
                                [
                                    -121.51702880859375,
                                    38.38472766885084
                                ],
                                [
                                    -121.5252685546875,
                                    38.377192010708846
                                ],
                                [
                                    -121.519775390625,
                                    38.36534867623904
                                ],
                                [
                                    -121.53076171875,
                                    38.352426464461466
                                ],
                                [
                                    -121.53900146484375,
                                    38.34704152882895
                                ],
                                [
                                    -121.55891418457031,
                                    38.34434891086139
                                ],
                                [
                                    -121.56852722167969,
                                    38.33465465738357
                                ],
                                [
                                    -121.57539367675781,
                                    38.326036454199986
                                ],
                                [
                                    -121.57814025878906,
                                    38.31741722616914
                                ],
                                [
                                    -121.57539367675781,
                                    38.3066417500507
                                ],
                                [
                                    -121.56166076660156,
                                    38.29101446582335
                                ],
                                [
                                    -121.51634216308594,
                                    38.25651475638941
                                ],
                                [
                                    -121.51016235351562,
                                    38.24950500026036
                                ],
                                [
                                    -121.51599884033203,
                                    38.24087667992996
                                ],
                                [
                                    -121.5249252319336,
                                    38.238989098339125
                                ],
                                [
                                    -121.53144836425781,
                                    38.23764079577116
                                ],
                                [
                                    -121.54243469238281,
                                    38.2400677223917
                                ],
                                [
                                    -121.54998779296875,
                                    38.24195527597933
                                ],
                                [
                                    -121.55754089355469,
                                    38.23952841236561
                                ],
                                [
                                    -121.5585708618164,
                                    38.23251701799486
                                ],
                                [
                                    -121.55582427978516,
                                    38.224695819045735
                                ],
                                [
                                    -121.55651092529297,
                                    38.21525532102998
                                ],
                                [
                                    -121.5585708618164,
                                    38.201227176515395
                                ],
                                [
                                    -121.56131744384766,
                                    38.19529137845557
                                ],
                                [
                                    -121.57058715820312,
                                    38.191243965987326
                                ],
                                [
                                    -121.58123016357422,
                                    38.187736026569354
                                ],
                                [
                                    -121.59324645996094,
                                    38.172622971624826
                                ],
                                [
                                    -121.59496307373047,
                                    38.16749461578964
                                ],
                                [
                                    -121.60354614257812,
                                    38.16425546830192
                                ],
                                [
                                    -121.61212921142578,
                                    38.16263584058641
                                ],
                                [
                                    -121.62311553955078,
                                    38.16452540275638
                                ],
                                [
                                    -121.62860870361328,
                                    38.1669547678699
                                ],
                                [
                                    -121.63272857666016,
                                    38.17019379541814
                                ],
                                [
                                    -121.63822174072266,
                                    38.172622971624826
                                ],
                                [
                                    -121.64749145507812,
                                    38.17289287509456
                                ],
                                [
                                    -121.66053771972656,
                                    38.17559185481662
                                ],
                                [
                                    -121.67221069335938,
                                    38.171273439283084
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Water from some source like a spring, snow melt or a lake starts at this high point and begins to flow down to lower points.The water source is extracted in the form of a line string.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/river.png",
            "classification_type": "linestring_geojson",
            "asset_category": "natural_elements",
            "asset_name": "River / Water Stream",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a98e4d3a8c00930325db36",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "5efa2eca-1ade-36e9-e2ae-6b469c0d7c97",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2",
            "storefront_clicks": 23,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2563099861145,
                                    37.806325533231224
                                ],
                                [
                                    -122.25613832473755,
                                    37.80644420718176
                                ],
                                [
                                    -122.25607395172119,
                                    37.80681718121327
                                ],
                                [
                                    -122.25568771362305,
                                    37.80703757407374
                                ],
                                [
                                    -122.25517272949219,
                                    37.80713929363364
                                ],
                                [
                                    -122.25459337234497,
                                    37.80713929363364
                                ],
                                [
                                    -122.25431442260742,
                                    37.807071480609274
                                ],
                                [
                                    -122.25386381149292,
                                    37.80713929363364
                                ],
                                [
                                    -122.25337028503418,
                                    37.80712234038338
                                ],
                                [
                                    -122.2529411315918,
                                    37.80703757407374
                                ],
                                [
                                    -122.25246906280518,
                                    37.80732577912956
                                ],
                                [
                                    -122.25206136703491,
                                    37.80761398306056
                                ],
                                [
                                    -122.25173950195312,
                                    37.80788523279169
                                ],
                                [
                                    -122.25163221359253,
                                    37.8081395285097
                                ],
                                [
                                    -122.25111722946167,
                                    37.80815648152641
                                ],
                                [
                                    -122.25053787231445,
                                    37.80825819954507
                                ],
                                [
                                    -122.25028038024902,
                                    37.808546399837006
                                ],
                                [
                                    -122.249915599823,
                                    37.80864811731856
                                ],
                                [
                                    -122.24957227706909,
                                    37.80847858810481
                                ],
                                [
                                    -122.24957227706909,
                                    37.808224293554424
                                ],
                                [
                                    -122.2497010231018,
                                    37.80798695118398
                                ],
                                [
                                    -122.24982976913452,
                                    37.80763093619796
                                ],
                                [
                                    -122.25006580352783,
                                    37.80732577912956
                                ],
                                [
                                    -122.25010871887207,
                                    37.807156246879984
                                ],
                                [
                                    -122.25030183792114,
                                    37.806935854373705
                                ],
                                [
                                    -122.25043058395386,
                                    37.80666460115535
                                ],
                                [
                                    -122.2504734992981,
                                    37.806325533231224
                                ],
                                [
                                    -122.25055932998657,
                                    37.80598646375031
                                ],
                                [
                                    -122.25068807601929,
                                    37.80564739271261
                                ],
                                [
                                    -122.25098848342896,
                                    37.80529136644753
                                ],
                                [
                                    -122.25113868713379,
                                    37.80500315345199
                                ],
                                [
                                    -122.25141763687134,
                                    37.80473189313463
                                ],
                                [
                                    -122.25186824798584,
                                    37.804545401088475
                                ],
                                [
                                    -122.2522759437561,
                                    37.804409770213645
                                ],
                                [
                                    -122.25268363952637,
                                    37.804358908571395
                                ],
                                [
                                    -122.25304841995239,
                                    37.80429109299388
                                ],
                                [
                                    -122.25330591201782,
                                    37.80395201417224
                                ],
                                [
                                    -122.25352048873901,
                                    37.803680749994065
                                ],
                                [
                                    -122.25369215011597,
                                    37.80334166837025
                                ],
                                [
                                    -122.2537350654602,
                                    37.80308735613073
                                ],
                                [
                                    -122.2539496421814,
                                    37.80290085993192
                                ],
                                [
                                    -122.25422859191895,
                                    37.802646546174586
                                ],
                                [
                                    -122.25457191467285,
                                    37.80230745980266
                                ],
                                [
                                    -122.25476503372192,
                                    37.801951417436676
                                ],
                                [
                                    -122.25476503372192,
                                    37.80171405490584
                                ],
                                [
                                    -122.25467920303345,
                                    37.80129019134637
                                ],
                                [
                                    -122.25463628768921,
                                    37.80090023472337
                                ],
                                [
                                    -122.25480794906616,
                                    37.80049332126961
                                ],
                                [
                                    -122.2551941871643,
                                    37.800289863702076
                                ],
                                [
                                    -122.25568771362305,
                                    37.800171179862225
                                ],
                                [
                                    -122.25624561309814,
                                    37.8001542250124
                                ],
                                [
                                    -122.25680351257324,
                                    37.8001542250124
                                ],
                                [
                                    -122.25733995437622,
                                    37.80005249583167
                                ],
                                [
                                    -122.25783348083496,
                                    37.799781217331464
                                ],
                                [
                                    -122.25826263427734,
                                    37.79957775780249
                                ],
                                [
                                    -122.25867033004761,
                                    37.799255612402135
                                ],
                                [
                                    -122.2587776184082,
                                    37.79900128609368
                                ],
                                [
                                    -122.2588849067688,
                                    37.798814779577626
                                ],
                                [
                                    -122.25905656814575,
                                    37.79867913817925
                                ],
                                [
                                    -122.25935697555542,
                                    37.79857740696702
                                ],
                                [
                                    -122.25972175598145,
                                    37.79862827259064
                                ],
                                [
                                    -122.26012945175171,
                                    37.79886564503779
                                ],
                                [
                                    -122.26027965545654,
                                    37.79888260018338
                                ],
                                [
                                    -122.26070880889893,
                                    37.79913692690048
                                ],
                                [
                                    -122.26111650466919,
                                    37.79930647755875
                                ],
                                [
                                    -122.26154565811157,
                                    37.79966253267431
                                ],
                                [
                                    -122.26163148880005,
                                    37.80006945070486
                                ],
                                [
                                    -122.26163148880005,
                                    37.8003915925559
                                ],
                                [
                                    -122.26163148880005,
                                    37.800985008077426
                                ],
                                [
                                    -122.26154565811157,
                                    37.80129019134637
                                ],
                                [
                                    -122.26148128509521,
                                    37.80178187284963
                                ],
                                [
                                    -122.26066589355469,
                                    37.80178187284963
                                ],
                                [
                                    -122.26070880889893,
                                    37.802154870427394
                                ],
                                [
                                    -122.26126670837402,
                                    37.802341368509886
                                ],
                                [
                                    -122.26105213165283,
                                    37.80296867678597
                                ],
                                [
                                    -122.26081609725952,
                                    37.803528163456
                                ],
                                [
                                    -122.26055860519409,
                                    37.80410459983463
                                ],
                                [
                                    -122.26075172424316,
                                    37.80461321643248
                                ],
                                [
                                    -122.26111650466919,
                                    37.80486752341785
                                ],
                                [
                                    -122.26163148880005,
                                    37.805104875814145
                                ],
                                [
                                    -122.26210355758667,
                                    37.80544394934273
                                ],
                                [
                                    -122.26231813430786,
                                    37.80600341726133
                                ],
                                [
                                    -122.26279020309448,
                                    37.80651202078214
                                ],
                                [
                                    -122.26300477981567,
                                    37.80710538712925
                                ],
                                [
                                    -122.26306915283203,
                                    37.80761398306056
                                ],
                                [
                                    -122.2629189491272,
                                    37.80832601147965
                                ],
                                [
                                    -122.26263999938965,
                                    37.80888545756392
                                ],
                                [
                                    -122.26261854171753,
                                    37.80942794668876
                                ],
                                [
                                    -122.26231813430786,
                                    37.810072147347995
                                ],
                                [
                                    -122.26203918457031,
                                    37.810512912981835
                                ],
                                [
                                    -122.26148128509521,
                                    37.810512912981835
                                ],
                                [
                                    -122.26113796234131,
                                    37.81008909992103
                                ],
                                [
                                    -122.26126670837402,
                                    37.809377088502615
                                ],
                                [
                                    -122.26148128509521,
                                    37.80830905850184
                                ],
                                [
                                    -122.26152420043945,
                                    37.80746140464897
                                ],
                                [
                                    -122.26133108139038,
                                    37.806918901076756
                                ],
                                [
                                    -122.26060152053833,
                                    37.80659678769507
                                ],
                                [
                                    -122.26025819778442,
                                    37.80617295215748
                                ],
                                [
                                    -122.26012945175171,
                                    37.80583388197601
                                ],
                                [
                                    -122.25974321365356,
                                    37.80554567109773
                                ],
                                [
                                    -122.2592282295227,
                                    37.80496924596682
                                ],
                                [
                                    -122.25918531417847,
                                    37.804833615870386
                                ],
                                [
                                    -122.2587776184082,
                                    37.80463017025873
                                ],
                                [
                                    -122.2579836845398,
                                    37.80461321643248
                                ],
                                [
                                    -122.25768327713013,
                                    37.804918384709865
                                ],
                                [
                                    -122.25738286972046,
                                    37.8045623549303
                                ],
                                [
                                    -122.25703954696655,
                                    37.80471493933172
                                ],
                                [
                                    -122.25729703903198,
                                    37.805104875814145
                                ],
                                [
                                    -122.25667476654053,
                                    37.805172690644405
                                ],
                                [
                                    -122.25643873214722,
                                    37.80541004205992
                                ],
                                [
                                    -122.25628852844238,
                                    37.80564739271261
                                ],
                                [
                                    -122.25643873214722,
                                    37.80598646375031
                                ],
                                [
                                    -122.2563099861145,
                                    37.806325533231224
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A lake is a large body of water surrounded by land on all sides.The lake outline is extracted in the form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/lake.png",
            "classification_type": "polygon_geojson",
            "asset_category": "natural_elements",
            "asset_name": "Lake / Water Body",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a98e2f3a8c00930325db35",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "e9c18512-f4fb-d407-6e8e-a03413924f67",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "1.5",
            "storefront_clicks": 16,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.51558303833008,
                                    37.78034830365159
                                ],
                                [
                                    -122.51266479492188,
                                    37.754972691904946
                                ],
                                [
                                    -122.50957489013672,
                                    37.733525549595306
                                ],
                                [
                                    -122.50528335571289,
                                    37.71397337431598
                                ],
                                [
                                    -122.5001335144043,
                                    37.695638522914486
                                ],
                                [
                                    -122.49635696411133,
                                    37.68042394494965
                                ],
                                [
                                    -122.49532699584961,
                                    37.67200024720591
                                ],
                                [
                                    -122.49652862548828,
                                    37.667923921096936
                                ],
                                [
                                    -122.49601364135742,
                                    37.664798586116575
                                ],
                                [
                                    -122.4949836730957,
                                    37.66276025327321
                                ],
                                [
                                    -122.49395370483398,
                                    37.65664491891393
                                ],
                                [
                                    -122.49395370483398,
                                    37.649169937299966
                                ],
                                [
                                    -122.49446868896484,
                                    37.64237384653978
                                ],
                                [
                                    -122.49567031860352,
                                    37.63163475580643
                                ],
                                [
                                    -122.49618530273438,
                                    37.6244291794224
                                ],
                                [
                                    -122.49670028686523,
                                    37.62089411297094
                                ],
                                [
                                    -122.49893188476562,
                                    37.62007830453267
                                ],
                                [
                                    -122.49773025512695,
                                    37.618446660800096
                                ],
                                [
                                    -122.49773025512695,
                                    37.61572717500684
                                ],
                                [
                                    -122.49858856201172,
                                    37.614095435788364
                                ],
                                [
                                    -122.49755859375,
                                    37.61232767789535
                                ],
                                [
                                    -122.4979019165039,
                                    37.61015191818638
                                ],
                                [
                                    -122.49893188476562,
                                    37.60784010375065
                                ],
                                [
                                    -122.50202178955078,
                                    37.60784010375065
                                ],
                                [
                                    -122.50202178955078,
                                    37.60580020781013
                                ],
                                [
                                    -122.50133514404297,
                                    37.6038962544593
                                ],
                                [
                                    -122.50185012817383,
                                    37.602400256925435
                                ],
                                [
                                    -122.50528335571289,
                                    37.59804809304534
                                ],
                                [
                                    -122.5082015991211,
                                    37.59627995374781
                                ],
                                [
                                    -122.51523971557617,
                                    37.59900015064849
                                ],
                                [
                                    -122.51729965209961,
                                    37.59832011074648
                                ],
                                [
                                    -122.51523971557617,
                                    37.59723203397509
                                ],
                                [
                                    -122.51729965209961,
                                    37.59587191563547
                                ],
                                [
                                    -122.52124786376953,
                                    37.594919818005295
                                ],
                                [
                                    -122.52038955688477,
                                    37.59274354910639
                                ],
                                [
                                    -122.51781463623047,
                                    37.59097528376386
                                ],
                                [
                                    -122.51729965209961,
                                    37.58662245162144
                                ],
                                [
                                    -122.51850128173828,
                                    37.58335766046065
                                ],
                                [
                                    -122.51850128173828,
                                    37.57995668407727
                                ],
                                [
                                    -122.51815795898438,
                                    37.57669160058645
                                ],
                                [
                                    -122.52021789550781,
                                    37.57519505613678
                                ],
                                [
                                    -122.52159118652344,
                                    37.573698481610705
                                ],
                                [
                                    -122.51832962036133,
                                    37.57342637392012
                                ],
                                [
                                    -122.51747131347656,
                                    37.57016100408264
                                ],
                                [
                                    -122.51609802246094,
                                    37.56621515784935
                                ],
                                [
                                    -122.51575469970703,
                                    37.562677324896235
                                ],
                                [
                                    -122.51489639282227,
                                    37.55737026044851
                                ],
                                [
                                    -122.51489639282227,
                                    37.55002139332707
                                ],
                                [
                                    -122.51592636108398,
                                    37.545257851658405
                                ],
                                [
                                    -122.51798629760742,
                                    37.54171902364639
                                ],
                                [
                                    -122.51935958862305,
                                    37.53763555182608
                                ],
                                [
                                    -122.51953125,
                                    37.5334157293421
                                ],
                                [
                                    -122.51781463623047,
                                    37.529059533095825
                                ],
                                [
                                    -122.51609802246094,
                                    37.52443079581378
                                ],
                                [
                                    -122.51747131347656,
                                    37.5217078750146
                                ],
                                [
                                    -122.51472473144531,
                                    37.51816792940794
                                ],
                                [
                                    -122.51335144042969,
                                    37.5128576961103
                                ],
                                [
                                    -122.50991821289062,
                                    37.51013435297773
                                ],
                                [
                                    -122.50545501708984,
                                    37.50673003432908
                                ],
                                [
                                    -122.50150680541992,
                                    37.50427882876826
                                ],
                                [
                                    -122.49944686889648,
                                    37.500738056428716
                                ],
                                [
                                    -122.50167846679688,
                                    37.49706092281259
                                ],
                                [
                                    -122.49910354614258,
                                    37.49420080481394
                                ],
                                [
                                    -122.49446868896484,
                                    37.49501799256211
                                ],
                                [
                                    -122.49584197998047,
                                    37.49937617620248
                                ],
                                [
                                    -122.48880386352539,
                                    37.5027808301943
                                ],
                                [
                                    -122.48193740844727,
                                    37.50032949496897
                                ],
                                [
                                    -122.47798919677734,
                                    37.502236095988195
                                ],
                                [
                                    -122.47335433959961,
                                    37.498967607290986
                                ],
                                [
                                    -122.47146606445312,
                                    37.50060186952387
                                ],
                                [
                                    -122.46528625488281,
                                    37.496924729201226
                                ],
                                [
                                    -122.45859146118164,
                                    37.490523349295856
                                ],
                                [
                                    -122.45138168334961,
                                    37.48044346899788
                                ],
                                [
                                    -122.44726181030273,
                                    37.470498470798724
                                ],
                                [
                                    -122.4452018737793,
                                    37.455782895709184
                                ],
                                [
                                    -122.4452018737793,
                                    37.448424022126545
                                ],
                                [
                                    -122.44485855102539,
                                    37.43942886015505
                                ],
                                [
                                    -122.44382858276367,
                                    37.43506718024053
                                ],
                                [
                                    -122.44314193725586,
                                    37.432477312518465
                                ],
                                [
                                    -122.4400520324707,
                                    37.43002367096198
                                ],
                                [
                                    -122.44073867797852,
                                    37.42620673534935
                                ],
                                [
                                    -122.43627548217773,
                                    37.42266226374488
                                ],
                                [
                                    -122.43524551391602,
                                    37.418572280320035
                                ],
                                [
                                    -122.43473052978516,
                                    37.41734524173289
                                ],
                                [
                                    -122.43301391601562,
                                    37.41420938511824
                                ],
                                [
                                    -122.43078231811523,
                                    37.411209747241
                                ],
                                [
                                    -122.42769241333008,
                                    37.41025529181653
                                ],
                                [
                                    -122.42734909057617,
                                    37.40793727801396
                                ],
                                [
                                    -122.42786407470703,
                                    37.406164630829345
                                ],
                                [
                                    -122.42683410644531,
                                    37.404391941703665
                                ],
                                [
                                    -122.42374420166016,
                                    37.4011191746703
                                ],
                                [
                                    -122.42305755615234,
                                    37.39607337878013
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "The line along which a large body of water like ocean, sea, river or lake meets the land.The shoreline is extracted in the form of a line string.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/shore.png",
            "classification_type": "linestring_geojson",
            "asset_category": "natural_elements",
            "asset_name": "Shoreline",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a98e673a8c00930325db37",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "0d4af62f-6ceb-7bd9-8a40-d5408bd71723",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4.5",
            "storefront_clicks": 54,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.30016678571701,
                                    37.8487095713269
                                ],
                                [
                                    -122.30001926422119,
                                    37.84881440504982
                                ],
                                [
                                    -122.29992002248764,
                                    37.84886532423287
                                ],
                                [
                                    -122.29978859424591,
                                    37.84892223386644
                                ],
                                [
                                    -122.29966521263123,
                                    37.848967162493494
                                ],
                                [
                                    -122.29953914880753,
                                    37.84900310537543
                                ],
                                [
                                    -122.29940503835678,
                                    37.849024072048465
                                ],
                                [
                                    -122.29926019906998,
                                    37.84903904823983
                                ],
                                [
                                    -122.29915022850037,
                                    37.84904204347774
                                ],
                                [
                                    -122.29902416467667,
                                    37.8490300625254
                                ],
                                [
                                    -122.29886323213577,
                                    37.84900909585404
                                ],
                                [
                                    -122.29870766401291,
                                    37.84897914345609
                                ],
                                [
                                    -122.29854941368103,
                                    37.848955181528964
                                ],
                                [
                                    -122.29836165904999,
                                    37.848910252894605
                                ],
                                [
                                    -122.29816049337387,
                                    37.848862328987785
                                ],
                                [
                                    -122.29800760746002,
                                    37.84882638603723
                                ],
                                [
                                    -122.29773670434952,
                                    37.84877247157852
                                ],
                                [
                                    -122.29754358530045,
                                    37.84874850958425
                                ],
                                [
                                    -122.2973558306694,
                                    37.84873353333387
                                ],
                                [
                                    -122.29716271162033,
                                    37.84872754283287
                                ],
                                [
                                    -122.29702323675156,
                                    37.84873652858417
                                ],
                                [
                                    -122.29679524898529,
                                    37.848754500083544
                                ],
                                [
                                    -122.29664236307144,
                                    37.84876947632969
                                ],
                                [
                                    -122.29653239250183,
                                    37.84878744782103
                                ],
                                [
                                    -122.29643851518631,
                                    37.84881140980266
                                ],
                                [
                                    -122.29646533727646,
                                    37.848892281433194
                                ],
                                [
                                    -122.29671478271484,
                                    37.8488383670227
                                ],
                                [
                                    -122.29696422815323,
                                    37.848793438317145
                                ],
                                [
                                    -122.29712784290314,
                                    37.84879044306916
                                ],
                                [
                                    -122.29734510183334,
                                    37.84879643356503
                                ],
                                [
                                    -122.29752749204636,
                                    37.848808414555386
                                ],
                                [
                                    -122.2976964712143,
                                    37.84883237653019
                                ],
                                [
                                    -122.29790300130844,
                                    37.84887131472268
                                ],
                                [
                                    -122.2980585694313,
                                    37.848907257651334
                                ],
                                [
                                    -122.29824900627136,
                                    37.84895817677028
                                ],
                                [
                                    -122.29844480752945,
                                    37.84901508633217
                                ],
                                [
                                    -122.29861110448837,
                                    37.849057019665494
                                ],
                                [
                                    -122.29874789714813,
                                    37.84908697203179
                                ],
                                [
                                    -122.29890614748001,
                                    37.849107938681016
                                ],
                                [
                                    -122.29905098676682,
                                    37.84912591008986
                                ],
                                [
                                    -122.2992092370987,
                                    37.84913489579261
                                ],
                                [
                                    -122.29933798313141,
                                    37.8491319005585
                                ],
                                [
                                    -122.29946941137314,
                                    37.84911093391612
                                ],
                                [
                                    -122.29956597089767,
                                    37.849089967267766
                                ],
                                [
                                    -122.29967594146729,
                                    37.84906301013971
                                ],
                                [
                                    -122.29981273412704,
                                    37.84901209109317
                                ],
                                [
                                    -122.29993879795074,
                                    37.84895817677028
                                ],
                                [
                                    -122.30005949735641,
                                    37.84890126716445
                                ],
                                [
                                    -122.30018019676208,
                                    37.848820395543754
                                ],
                                [
                                    -122.30026870965958,
                                    37.84874850958425
                                ],
                                [
                                    -122.300343811512,
                                    37.848667637796005
                                ],
                                [
                                    -122.30043232440948,
                                    37.84854483231837
                                ],
                                [
                                    -122.30048060417175,
                                    37.84847294609019
                                ],
                                [
                                    -122.30053961277008,
                                    37.84835912608562
                                ],
                                [
                                    -122.30058252811432,
                                    37.84826028225489
                                ],
                                [
                                    -122.30061739683151,
                                    37.84816143829165
                                ],
                                [
                                    -122.30063885450363,
                                    37.84804162724948
                                ],
                                [
                                    -122.30064690113068,
                                    37.84789186317299
                                ],
                                [
                                    -122.30062544345856,
                                    37.847730117628785
                                ],
                                [
                                    -122.30059862136841,
                                    37.84761030588577
                                ],
                                [
                                    -122.3005422949791,
                                    37.847466531537215
                                ],
                                [
                                    -122.30047255754471,
                                    37.84734971467261
                                ],
                                [
                                    -122.30038672685623,
                                    37.84723888824537
                                ],
                                [
                                    -122.30029553174973,
                                    37.84712806165157
                                ],
                                [
                                    -122.3002016544342,
                                    37.84702023021123
                                ],
                                [
                                    -122.3001104593277,
                                    37.84694235185067
                                ],
                                [
                                    -122.30001389980316,
                                    37.84687345938628
                                ],
                                [
                                    -122.29991734027863,
                                    37.84679857619977
                                ],
                                [
                                    -122.29983150959015,
                                    37.8469034126396
                                ],
                                [
                                    -122.30002462863922,
                                    37.84703221149015
                                ],
                                [
                                    -122.30023115873337,
                                    37.84719395856524
                                ],
                                [
                                    -122.30038940906525,
                                    37.847382663037784
                                ],
                                [
                                    -122.3004886507988,
                                    37.84757735762233
                                ],
                                [
                                    -122.30055570602417,
                                    37.84778103756059
                                ],
                                [
                                    -122.30055838823318,
                                    37.84796974053073
                                ],
                                [
                                    -122.30053156614304,
                                    37.84818839574932
                                ],
                                [
                                    -122.30044573545456,
                                    37.84838009294175
                                ],
                                [
                                    -122.30035990476608,
                                    37.84851487973182
                                ],
                                [
                                    -122.30026870965958,
                                    37.848625704241286
                                ],
                                [
                                    -122.30016678571701,
                                    37.8487095713269
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "An interchange is a road junction that typically uses grade separation, and one or more ramps, to permit traffic on at least one highway to pass through the junction without directly crossing any other traffic stream.The interchange is extracted in the form of polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/interchange.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Interchange",
            "visible": "1",
            "quality_metrics": "91%",
            "processing_units_sqkm": "18",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9485695aa33b377dd1920",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "a6dd2cbb-8842-a4aa-6288-b3bd04513241",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    77.22359495703131,
                                    28.635140992946315
                                ],
                                [
                                    77.22329991403967,
                                    28.635402302818942
                                ],
                                [
                                    77.22303705755621,
                                    28.635675382711206
                                ],
                                [
                                    77.22276079002768,
                                    28.635988482057957
                                ],
                                [
                                    77.22259449306875,
                                    28.636190936777293
                                ],
                                [
                                    77.22215729299933,
                                    28.636835964324796
                                ],
                                [
                                    77.22191052976996,
                                    28.63718437171571
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    77.22187566105276,
                                    28.63717495531496
                                ],
                                [
                                    77.2225488955155,
                                    28.63616974967383
                                ],
                                [
                                    77.22272055689245,
                                    28.635979065549886
                                ],
                                [
                                    77.22324895206839,
                                    28.635395240398495
                                ],
                                [
                                    77.22357349935919,
                                    28.635112743191367
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    77.22349571529776,
                                    28.635093910017186
                                ],
                                [
                                    77.2231067949906,
                                    28.635461156304075
                                ],
                                [
                                    77.22290294710547,
                                    28.635687153380232
                                ],
                                [
                                    77.22271519247442,
                                    28.63592256648364
                                ],
                                [
                                    77.22248452249914,
                                    28.636157979058922
                                ],
                                [
                                    77.22183811012655,
                                    28.63715376841017
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "OHE line consists of overhead wires situated over rail tracks.The OHE line is extracted in the form of a line string.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/ohe_wire.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Overhead Wire",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad0d19e0a32610bedc077",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "cc07b3d8-8ef1-8308-a4c3-41d6fb48bb0c",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 11,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29209071723744,
                                    37.839781684251165
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2920826706104,
                                    37.83982811625329
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Rail signs are erected at the side of the railway track typically on a pole to give instructions or provide information to railway drivers. The point at the top or bottom of the railway sign is annotated. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/rail_sign_location.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Rail Sign Location",
            "visible": "1",
            "quality_metrics": "2cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad29f9e0a32610bedc07f",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "4c142976-c8ea-4926-921d-6758157f7e65",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 7,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27577752200887,
                                    37.876379002351996
                                ],
                                [
                                    -122.27577483979985,
                                    37.87636552877911
                                ],
                                [
                                    -122.27576142875478,
                                    37.87637001997035
                                ],
                                [
                                    -122.27576142875478,
                                    37.8763864876692
                                ],
                                [
                                    -122.27577752200887,
                                    37.876379002351996
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "A transformer is an electrical device that transfers electrical energy between two or more circuits through electromagnetic induction. The outline of the transformer is extracted in the form of a polygon.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/transformer.png",
            "classification_type": "polygon_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Transformer Outline",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a973e795aa33b377dd192a",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "ba021c1b-d015-f020-c2af-567cdb354bfc",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/83dc983c15f50a98455a",
            "processing_units_km": "4",
            "storefront_clicks": 10,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Id": 8,
                            "Name": "Roof_Feature"
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.30787285423804,
                                        38.32265381230924,
                                        27.98888898588805
                                    ],
                                    [
                                        -122.30785673121765,
                                        38.32266660041598,
                                        27.995542044205283
                                    ],
                                    [
                                        -122.3078404982387,
                                        38.32265389865753,
                                        27.965316510968083
                                    ],
                                    [
                                        -122.3078566216076,
                                        38.32264111055218,
                                        27.900471667404222
                                    ],
                                    [
                                        -122.30787285423804,
                                        38.32265381230924,
                                        27.98888898588805
                                    ]
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Building roof features are features on the roof such as a chimney, skylight, HVAC equipment etc.The roof features are extracted in form of a polygon. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/building_roof_feature.png",
            "classification_type": "polygon_geojson",
            "asset_category": "buildings",
            "asset_name": "Building Roof Feature",
            "visible": "1",
            "quality_metrics": "91%",
            "processing_units_sqkm": "16",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97a7a85de584c02741c70",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100"
            ]
        },
        {
            "asset_id": "bf586d07-f7a6-eb52-cd5d-ecb3df69f63c",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 9,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    77.21145796123892,
                                    28.665175594509666
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    77.21135067287832,
                                    28.66500143745204
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Overhead line pole is deployed to support one or more overhead wires situated over rail tracks, raised to a high electrical potential by connection to feeder stations at regular intervals. The top or base of the pole is annotated as points. ",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/ohe_pole.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "Rail Overhead Pole / Gantry",
            "visible": "1",
            "quality_metrics": "3cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad1ae9e0a32610bedc07b",
            "output_format": "geojson",
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "6cbbe6f8-35aa-7013-195f-6f1c3e1b62a0",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/storage_tank.png",
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.400882455986,
                                    37.93151356612131
                                ],
                                [
                                    -122.40083551732823,
                                    37.93151991286673
                                ],
                                [
                                    -122.4007939430885,
                                    37.93151673949407
                                ],
                                [
                                    -122.40072957007214,
                                    37.93151250833035
                                ],
                                [
                                    -122.40068397251889,
                                    37.93149452588175
                                ],
                                [
                                    -122.40064239827916,
                                    37.93146808109645
                                ],
                                [
                                    -122.40061557618901,
                                    37.93144480967749
                                ],
                                [
                                    -122.40059143630788,
                                    37.93141201812007
                                ],
                                [
                                    -122.40057266084477,
                                    37.93137605316936
                                ],
                                [
                                    -122.40056193200871,
                                    37.931336914820626
                                ],
                                [
                                    -122.40055790869519,
                                    37.931293545274734
                                ],
                                [
                                    -122.40056997863576,
                                    37.93125229129267
                                ],
                                [
                                    -122.40060350624844,
                                    37.93120574831077
                                ],
                                [
                                    -122.40064508048818,
                                    37.93117189885091
                                ],
                                [
                                    -122.40068799583241,
                                    37.931148627338196
                                ],
                                [
                                    -122.40074566332623,
                                    37.93113170259705
                                ],
                                [
                                    -122.4008113774471,
                                    37.93112535581812
                                ],
                                [
                                    -122.40086770383641,
                                    37.93113170259705
                                ],
                                [
                                    -122.40091598359868,
                                    37.93114439615328
                                ],
                                [
                                    -122.40095889894292,
                                    37.93116555207545
                                ],
                                [
                                    -122.4009924265556,
                                    37.93119199696958
                                ],
                                [
                                    -122.40102058975026,
                                    37.93122267303489
                                ],
                                [
                                    -122.4010447296314,
                                    37.93125969585526
                                ],
                                [
                                    -122.40105411736295,
                                    37.93128931409814
                                ],
                                [
                                    -122.40105948178098,
                                    37.93132951026582
                                ],
                                [
                                    -122.40105679957196,
                                    37.93136335965313
                                ],
                                [
                                    -122.40103534189984,
                                    37.93140884474278
                                ],
                                [
                                    -122.40099510876462,
                                    37.93145432980432
                                ],
                                [
                                    -122.40095353452489,
                                    37.93148183238598
                                ],
                                [
                                    -122.40091061918065,
                                    37.931499814837686
                                ],
                                [
                                    -122.400882455986,
                                    37.93151356612131
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.40050158230588,
                                    37.93149875704652
                                ],
                                [
                                    -122.4004868301563,
                                    37.93152308623921
                                ],
                                [
                                    -122.40046671358868,
                                    37.931544242052404
                                ],
                                [
                                    -122.40044391481206,
                                    37.93155905111804
                                ],
                                [
                                    -122.40041843382642,
                                    37.93156751343989
                                ],
                                [
                                    -122.40038222400472,
                                    37.93157809134086
                                ],
                                [
                                    -122.40034064976498,
                                    37.93157809134086
                                ],
                                [
                                    -122.40029907552525,
                                    37.931565397859515
                                ],
                                [
                                    -122.40026823012158,
                                    37.93154635763339
                                ],
                                [
                                    -122.400253477972,
                                    37.931529432983815
                                ],
                                [
                                    -122.40023872582242,
                                    37.93150404600214
                                ],
                                [
                                    -122.40022933809087,
                                    37.93147442784578
                                ],
                                [
                                    -122.40022933809087,
                                    37.93145221422066
                                ],
                                [
                                    -122.40023738471791,
                                    37.931430000588875
                                ],
                                [
                                    -122.40024945465848,
                                    37.93140567136534
                                ],
                                [
                                    -122.4002722534351,
                                    37.93138980447612
                                ],
                                [
                                    -122.40030309883878,
                                    37.931372879790494
                                ],
                                [
                                    -122.40033394424245,
                                    37.9313654752393
                                ],
                                [
                                    -122.40036478964612,
                                    37.93136124406688
                                ],
                                [
                                    -122.40040099946782,
                                    37.93136124406688
                                ],
                                [
                                    -122.40043855039403,
                                    37.93137182199752
                                ],
                                [
                                    -122.40046671358868,
                                    37.931390862268834
                                ],
                                [
                                    -122.4004868301563,
                                    37.93140884474278
                                ],
                                [
                                    -122.40050292341039,
                                    37.93143423175733
                                ],
                                [
                                    -122.40050694672391,
                                    37.93146067655482
                                ],
                                [
                                    -122.40050158230588,
                                    37.93149875704652
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.40050158230588,
                                    37.931500872628824
                                ],
                                [
                                    -122.4005056056194,
                                    37.93147865901172
                                ],
                                [
                                    -122.40050158230588,
                                    37.931500872628824
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.39873802987859,
                                        37.93080590057063
                                    ],
                                    [
                                        -122.39869243232533,
                                        37.93084292360089
                                    ],
                                    [
                                        -122.398633423727,
                                        37.93086831081088
                                    ],
                                    [
                                        -122.39857977954671,
                                        37.930885235612635
                                    ],
                                    [
                                        -122.3985234531574,
                                        37.9308915824123
                                    ],
                                    [
                                        -122.39847249118611,
                                        37.93088417781264
                                    ],
                                    [
                                        -122.39842152921483,
                                        37.930866195210385
                                    ],
                                    [
                                        -122.3983812960796,
                                        37.93084821260369
                                    ],
                                    [
                                        -122.39834776846692,
                                        37.9308270565902
                                    ],
                                    [
                                        -122.39831826416776,
                                        37.93080061156478
                                    ],
                                    [
                                        -122.39829546539113,
                                        37.93076887752172
                                    ],
                                    [
                                        -122.39828071324155,
                                        37.93073079665197
                                    ],
                                    [
                                        -122.39827803103253,
                                        37.93068319553705
                                    ],
                                    [
                                        -122.39828205434605,
                                        37.930642999015966
                                    ],
                                    [
                                        -122.39829412428662,
                                        37.930610207100955
                                    ],
                                    [
                                        -122.39831692306325,
                                        37.93057847297568
                                    ],
                                    [
                                        -122.39833838073537,
                                        37.930556259079886
                                    ],
                                    [
                                        -122.39836922613904,
                                        37.93053721859239
                                    ],
                                    [
                                        -122.39840945927426,
                                        37.930517120294724
                                    ],
                                    [
                                        -122.39844835130498,
                                        37.93050442663018
                                    ],
                                    [
                                        -122.39848992554471,
                                        37.930495964185944
                                    ],
                                    [
                                        -122.39853418199345,
                                        37.930495964185944
                                    ],
                                    [
                                        -122.39858916727826,
                                        37.930502311019225
                                    ],
                                    [
                                        -122.39863878814504,
                                        37.930515004684096
                                    ],
                                    [
                                        -122.3986870679073,
                                        37.93053721859239
                                    ],
                                    [
                                        -122.39872595993802,
                                        37.930565779321775
                                    ],
                                    [
                                        -122.39875412313268,
                                        37.930600686864814
                                    ],
                                    [
                                        -122.39877155749127,
                                        37.93062607415844
                                    ],
                                    [
                                        -122.39878228632733,
                                        37.93065992386963
                                    ],
                                    [
                                        -122.39877960411832,
                                        37.93070329378932
                                    ],
                                    [
                                        -122.39877826301381,
                                        37.93073397005855
                                    ],
                                    [
                                        -122.39876351086423,
                                        37.930763588513216
                                    ],
                                    [
                                        -122.39873802987859,
                                        37.93080590057063
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Storage tanks are containers that hold liquids, compressed gases (gas tank) or mediums used for the short- or long-term storage of heat or cold.The outline of the base of storage tank is extracted in the form of a polygon. ",
            "processing_units_km": "4",
            "classification_type": "polygon_geojson",
            "asset_category": "oil_gas_distribution",
            "asset_name": "Storage Tank",
            "visible": "0",
            "quality_metrics": "90%",
            "processing_units_sqkm": "16",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9785995aa33b377dd192d",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "18dd05c0-65af-c42b-7244-ba5daa01712a",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/00f3be4264caa13b280b",
            "processing_units_km": "2",
            "storefront_clicks": 10,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "Shoulder Line"
                        },
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -117.2010271898065,
                                    34.06621960138544,
                                    371.75790450928383
                                ],
                                [
                                    -117.201077540177,
                                    34.066333046551115,
                                    371.7094973474802
                                ],
                                [
                                    -117.2011447585013,
                                    34.066497377822266,
                                    371.6739986737401
                                ],
                                [
                                    -117.20120790170478,
                                    34.06666333205444,
                                    371.6217198275862
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Shoulder or edge line rumble strips are one of the proven countermeasures that reduce the risks of run-off-road crashes.The shoulder line is extracted in the form of a line string.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/shoulder_line.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "Shoulder Line",
            "visible": "1",
            "quality_metrics": "94%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9494f95aa33b377dd1923",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "5ce06346-5c5b-bdff-6049-0e43bd48e2d6",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/e3f7b9a71d7fcd0df6c3",
            "processing_units_km": "1.5",
            "storefront_clicks": 11,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "transmission_poles"
                        },
                        "geometry": {
                            "type": "MultiPoint",
                            "coordinates": [
                                [
                                    -120.7903325038474,
                                    35.34181904108948,
                                    253.26
                                ],
                                [
                                    -120.78801571046871,
                                    35.34010451356076,
                                    331.28
                                ],
                                [
                                    -120.78757625794637,
                                    35.33977536433501,
                                    311.5
                                ],
                                [
                                    -120.78852371667978,
                                    35.34047367917041,
                                    307.76
                                ],
                                [
                                    -120.78524850468747,
                                    35.338063559467116,
                                    215.55
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Transmission towers support the high-voltage conductors and wires of overhead power lines from the generating station switchyard right up to the source substations. The lowest point of the transmission tower is annotated in the form of a point.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/transmission_tower_bot.png",
            "classification_type": "points_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Transmission Tower (bottom)",
            "visible": "1",
            "quality_metrics": "3cm",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9736095aa33b377dd1925",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "a3321da1-e1a9-111b-f35b-bbe143824064",
            "asset_image_1_url": "",
            "geojson_iframe": "http://bl.ocks.org/d/51b668160261890e5693",
            "processing_units_km": "1.5",
            "storefront_clicks": 14,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "transmission_poles"
                        },
                        "geometry": {
                            "type": "MultiPoint",
                            "coordinates": [
                                [
                                    -120.7903325038474,
                                    35.34181904108948,
                                    263.26
                                ],
                                [
                                    -120.78801571046871,
                                    35.34010451356076,
                                    341.28
                                ],
                                [
                                    -120.78757625794637,
                                    35.33977536433501,
                                    321.5
                                ],
                                [
                                    -120.78852371667978,
                                    35.34047367917041,
                                    317.76
                                ],
                                [
                                    -120.78524850468747,
                                    35.338063559467116,
                                    225.55
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Transmission towers support the high-voltage conductors and wires of overhead power lines from the generating station switchyard right up to the source substations. The highest point of the transmission tower is annotated in the form of a point.",
            "annotated_image_url": "https://s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/transmission_tower_top.png",
            "classification_type": "points_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "Power Transmission Tower (top)",
            "visible": "1",
            "quality_metrics": "3cm",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9737795aa33b377dd1926",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "377e0996-2799-8b64-1b24-c52fb9c0ac59",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2",
            "storefront_clicks": 38,
            "geojson_dict": "",
            "asset_description": "Water is a transparent fluid which forms the worlds streams,lakes,oceansandrain.ThepointsofwaterexposedonthesurfaceoftheearthareextractedintheformofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Water(9)",
            "visible": "1",
            "quality_metrics": "97%",
            "processing_units_sqkm": "8",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e263a8c00930325db30",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "fa5b819e-2ac4-1be1-8c72-8ca740657018",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29221269488335,
                                    37.839756221527935
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2922046482563,
                                    37.83982062722265
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Asignalisamechanicalorelectricaldeviceerectedbesidearailwaylinetopassinformationrelatingtothestateofthelineahead.Thepointatthetoporbottomoftherailwaysignalisannotated.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/rail_signal_location.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "RailSignalLocation",
            "visible": "1",
            "quality_metrics": "3cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad26d9e0a32610bedc07e",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "733ebc62-ddd0-6333-cef0-a66ab67af234",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3.5",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.29182310402393,
                                    37.83770869024602
                                ],
                                [
                                    -122.29193978011608,
                                    37.838201480949124
                                ],
                                [
                                    -122.29199878871441,
                                    37.83848007849169
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Railwaysignalingcableisasetofarmoredcablesthatrunparalleltotherailwaytrack.Thecablesaretypicallypassedthroughametalcableconduittoprotectthemforwearandtear.Thecontinuouscablesareextractedintheformofalinestring.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/signallingcable.png",
            "classification_type": "linestring_geojson",
            "asset_category": "transportation",
            "asset_name": "SignallingCable",
            "visible": "1",
            "quality_metrics": "96%",
            "processing_units_sqkm": "14",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad1e79e0a32610bedc07c",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "13114bea-32af-1b0e-8a3b-b3fcce575087",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3.5",
            "storefront_clicks": 11,
            "geojson_dict": "",
            "asset_description": "Wireclassificationreferstoclassifiedpointsonthewireslikepowerdistributionwires, telephonewires,            powertransmissionwires.TheoutputisintheformofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Wire(14)",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "14",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e553a8c00930325db33",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "0de6a711-1fd4-9aa7-4eff-2cacc1712161",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 10,
            "geojson_dict": "",
            "asset_description": "Along,slender,roundedpieceofwoodormetal, typicallyusedwithoneendplacedinthegroundasasupportforsomething.TheoutputisintheformofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Pole/Tower(15)",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e643a8c00930325db34",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "248dccb1-11af-b3c1-fde4-6dc7edca0765",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "1.5",
            "storefront_clicks": 68,
            "geojson_dict": "",
            "asset_description": "Groundreferstothesolidsurfaceoftheearth.Weextractallthepointsonthesurfaceoftheearththatareexposedanaerialormobiledatacollectionvehicle.ThegroundpointsareextractedintheformofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Ground(2)",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97ddb3a8c00930325db2d",
            "output_format": "classified_las_asprs",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "1e5ded57-fb47-d3b2-de9d-0ad66a6dab3d",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 5,
            "geojson_dict": "",
            "asset_description": "Adeviceforcontrollingthepassageoffluidthroughapipeorduct, especiallyanautomaticdeviceallowingmovementinonedirectiononly.Thevalveisannotatedintheformofapoint.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/valve.png",
            "classification_type": "points_geojson",
            "asset_category": "oil_gas_distribution",
            "asset_name": "Valve",
            "visible": "0",
            "quality_metrics": "5cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9742595aa33b377dd192c",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "40002f60-bd6f-84b1-f589-02daaca811f8",
            "asset_image_1_url": "",
            "geojson_iframe": "http: //bl.ocks.org/d/77ef9628a4661cccd2f6",
            "processing_units_km": "2.5",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "name": "Powerline"
                        },
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -120.78801767557992,
                                    35.33990028343692,
                                    357.21426035902465
                                ],
                                [
                                    -120.78798886390521,
                                    35.339878654744176,
                                    355.71
                                ],
                                [
                                    -120.78792861694112,
                                    35.339828692136784,
                                    351.78
                                ],
                                [
                                    -120.78786553501769,
                                    35.33978183273993,
                                    348.08
                                ],
                                [
                                    -120.7878021109978,
                                    35.33973541778242,
                                    344.42
                                ],
                                [
                                    -120.7877390218495,
                                    35.33968882862998,
                                    340.8
                                ],
                                [
                                    -120.78767481350263,
                                    35.33964294012875,
                                    337.23
                                ],
                                [
                                    -120.78761161699602,
                                    35.33959625879908,
                                    333.69
                                ],
                                [
                                    -120.7875480808495,
                                    35.339549931805955,
                                    330.19
                                ],
                                [
                                    -120.78748555654307,
                                    35.33950281198616,
                                    326.73
                                ],
                                [
                                    -120.78742247267417,
                                    35.33945604247609,
                                    323.31
                                ],
                                [
                                    -120.78735949883945,
                                    35.33940927494901,
                                    319.93
                                ],
                                [
                                    -120.78729675482985,
                                    35.33936215101115,
                                    316.58
                                ],
                                [
                                    -120.78723344634315,
                                    35.33931555758574,
                                    313.28
                                ],
                                [
                                    -120.78717081244083,
                                    35.33926843560082,
                                    310.02
                                ],
                                [
                                    -120.78710727434775,
                                    35.33922219849397,
                                    306.8
                                ],
                                [
                                    -120.78704441821218,
                                    35.33917516252233,
                                    303.61
                                ],
                                [
                                    -120.78698099759927,
                                    35.33912865706188,
                                    300.47
                                ],
                                [
                                    -120.78691813669425,
                                    35.339081801232986,
                                    297.37
                                ],
                                [
                                    -120.78685494106455,
                                    35.33903511953522,
                                    294.3
                                ],
                                [
                                    -120.78679174550794,
                                    35.33898843780672,
                                    291.28
                                ],
                                [
                                    -120.78672877240341,
                                    35.33894166997397,
                                    288.29
                                ],
                                [
                                    -120.78666580428819,
                                    35.33889472190693,
                                    285.35
                                ],
                                [
                                    -120.7866028313297,
                                    35.33884795401313,
                                    282.44
                                ],
                                [
                                    -120.78653963360709,
                                    35.3388013622639,
                                    279.58
                                ],
                                [
                                    -120.78647632599734,
                                    35.338754768469485,
                                    276.75
                                ],
                                [
                                    -120.78641312842075,
                                    35.33870817665869,
                                    273.96
                                ],
                                [
                                    -120.78634982095714,
                                    35.338661582802615,
                                    271.21
                                ],
                                [
                                    -120.78628685328077,
                                    35.33861463455193,
                                    268.51
                                ],
                                [
                                    -120.78622365346465,
                                    35.33856813275081,
                                    265.84
                                ],
                                [
                                    -120.78616046355623,
                                    35.33852127051149,
                                    263.21
                                ],
                                [
                                    -120.78609714900871,
                                    35.33847494683785,
                                    260.62
                                ],
                                [
                                    -120.7860340667475,
                                    35.33842817665365,
                                    258.07
                                ],
                                [
                                    -120.78597064730386,
                                    35.338381670699825,
                                    255.56
                                ],
                                [
                                    -120.78590744785237,
                                    35.338335168744834,
                                    253.09
                                ],
                                [
                                    -120.78584425585086,
                                    35.33828839645355,
                                    250.66
                                ],
                                [
                                    -120.78578094658585,
                                    35.33824189242205,
                                    248.27
                                ],
                                [
                                    -120.7857177498123,
                                    35.33819530027291,
                                    245.91
                                ],
                                [
                                    -120.78565421831547,
                                    35.33814888225131,
                                    243.6
                                ],
                                [
                                    -120.78559159115837,
                                    35.33810157930175,
                                    241.33
                                ],
                                [
                                    -120.78552974362589,
                                    35.33805393002138,
                                    239.1
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "PowerTransmissionwiresaresetofwiresthatconnectpowertransmissiontowers.Thewiresareextractedintheformoflinestrings.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/transmission_wire.png",
            "classification_type": "linestring_geojson",
            "asset_category": "power_transmission_distribution",
            "asset_name": "PowerTransmissionWire",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9732195aa33b377dd1924",
            "output_format": "geojson",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "2659ac07-ad90-59b0-1e9f-88283bf7be47",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4",
            "storefront_clicks": 48,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Id": 9,
                            "Name": "Roof_Planes"
                        },
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.307268771039,
                                        38.32363678815357,
                                        24.65868639549556
                                    ],
                                    [
                                        -122.30732697962782,
                                        38.32362898584868,
                                        27.573423531220744
                                    ],
                                    [
                                        -122.3073754817665,
                                        38.3236212098803,
                                        26.123468056231793
                                    ],
                                    [
                                        -122.30737871760093,
                                        38.32362120125842,
                                        26.097883983284028
                                    ],
                                    [
                                        -122.3075856464088,
                                        38.323584963443885,
                                        25.874944346265202
                                    ],
                                    [
                                        -122.30762899287076,
                                        38.323574035557805,
                                        27.180857877597088
                                    ],
                                    [
                                        -122.30766735434929,
                                        38.32356416993536,
                                        23.747622695746887
                                    ],
                                    [
                                        -122.30767950193778,
                                        38.323589811099694,
                                        25.24917640957091
                                    ],
                                    [
                                        -122.3076730414404,
                                        38.32359237763657,
                                        26.081105260277766
                                    ],
                                    [
                                        -122.30770263396518,
                                        38.3237019056329,
                                        25.81849351783324
                                    ],
                                    [
                                        -122.30763150369594,
                                        38.323714840193084,
                                        26.295505081277668
                                    ],
                                    [
                                        -122.30734052423685,
                                        38.323769144762444,
                                        27.074976437509928
                                    ],
                                    [
                                        -122.30720150025712,
                                        38.323795005188394,
                                        27.182896857027664
                                    ],
                                    [
                                        -122.30717193033237,
                                        38.32369057461132,
                                        25.11903622370259
                                    ],
                                    [
                                        -122.30726892420024,
                                        38.323672473506385,
                                        25.642669114066713
                                    ],
                                    [
                                        -122.307268771039,
                                        38.32363678815357,
                                        24.65868639549556
                                    ]
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Buildingroofplaneistheoutlineofaplaneofthebuildingroof.Theroofplaneisextractedinformofapolygon.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/building_roof_plane.png",
            "classification_type": "polygon_geojson",
            "asset_category": "buildings",
            "asset_name": "BuildingRoofPlane",
            "visible": "1",
            "quality_metrics": "90%",
            "processing_units_sqkm": "16",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97a6085de584c02741c6f",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "3cecb4e8-b881-2a5f-9107-f1e3d3962d94",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "4.5",
            "storefront_clicks": 73,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.47977018356325,
                                        37.82859051263622
                                    ],
                                    [
                                        -122.4795126914978,
                                        37.82862440927334
                                    ],
                                    [
                                        -122.47748494148254,
                                        37.810928247421295
                                    ],
                                    [
                                        -122.47776389122009,
                                        37.81089434265669
                                    ],
                                    [
                                        -122.47977018356325,
                                        37.82859051263622
                                    ]
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Astructurecarryingaroad, path, railroad, orcanalacrossariver,ravine,road,railroad,         orotherobstacle.Thebridgeisextractedintheformofapolygon.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/bridge.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "Bridge",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "18",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a947b495aa33b377dd191f",
            "output_format": "geojson",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "103e1e2c-0360-bb8d-2583-23e387dc9267",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 14,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.2915423521772,
                                        37.837166466884185
                                    ],
                                    [
                                        -122.29155039880425,
                                        37.837176951904105
                                    ],
                                    [
                                        -122.29153698775917,
                                        37.8371829433434
                                    ],
                                    [
                                        -122.29152625892311,
                                        37.83717245832434
                                    ],
                                    [
                                        -122.2915423521772,
                                        37.837166466884185
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.29143104050308,
                                        37.83721739410986
                                    ],
                                    [
                                        -122.2914162883535,
                                        37.83722937698137
                                    ],
                                    [
                                        -122.29140690062195,
                                        37.837208406954936
                                    ],
                                    [
                                        -122.29142433498055,
                                        37.83720241551773
                                    ],
                                    [
                                        -122.29143104050308,
                                        37.83721739410986
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.2913613030687,
                                        37.83722787912255
                                    ],
                                    [
                                        -122.29136666748673,
                                        37.83724135985095
                                    ],
                                    [
                                        -122.29135191533715,
                                        37.837245853426545
                                    ],
                                    [
                                        -122.29134520981461,
                                        37.83723087484019
                                    ],
                                    [
                                        -122.2913613030687,
                                        37.83722787912255
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [
                                [
                                    [
                                        -122.2913613030687,
                                        37.83722638126368
                                    ],
                                    [
                                        -122.2913613030687,
                                        37.83722638126368
                                    ]
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Railsignsareerectedatthesideoftherailwaytracktypicallyonapoletogiveinstructionsorprovideinformationtorailwaydrivers.Theoutlineoftheshapeofthesignalisextractedintheformofapolygon.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/rail_sign_outline.png",
            "classification_type": "polygon_geojson",
            "asset_category": "transportation",
            "asset_name": "RailSignOutline",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad1489e0a32610bedc079",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "ec49b0d3-edab-121f-84b5-88c2f3dea303",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "2.5",
            "storefront_clicks": 8,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.2907354682684,
                                    37.831857859704314
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Waysideboxistypicallyaboxcontainingrailwaysignalorcommunicationequipment.Thetoporbottompointofthewaysideboxlocationisannotatedasapoint.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/wayside_box_location.png",
            "classification_type": "points_geojson",
            "asset_category": "transportation",
            "asset_name": "WaysideBoxLocation",
            "visible": "1",
            "quality_metrics": "3cm",
            "processing_units_sqkm": "10",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55aad2c89e0a32610bedc080",
            "output_format": "geojson",
            "recommended_features": [
                "rgb"
            ],
            "survey_type": [
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "bf752457-618d-b698-b1b7-4441175bdd62",
            "asset_image_1_url": "",
            "geojson_iframe": "http: //bl.ocks.org/d/991e8622c1cd74a22254",
            "processing_units_km": "1.5",
            "storefront_clicks": 9,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "Name": "GeneralPetroleumLine"
                        },
                        "geometry": {
                            "type": "MultiLineString",
                            "coordinates": [
                                [
                                    [
                                        41.957699,
                                        36.949675,
                                        0
                                    ],
                                    [
                                        36.593541,
                                        34.314649,
                                        0
                                    ],
                                    [
                                        35.628933,
                                        33.985271,
                                        0
                                    ]
                                ],
                                [
                                    [
                                        36.546661,
                                        34.344162,
                                        0
                                    ],
                                    [
                                        35.911257,
                                        35.02046,
                                        0
                                    ]
                                ]
                            ]
                        }
                    }
                ]
            },
            "asset_description": "Aconduitmadefrompipesconnectedend-to-endforlong-distancefluidorgastransport.Thepipelineisextractedinthefromofalinestring.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/pipeline.png",
            "classification_type": "linestring_geojson",
            "asset_category": "oil_gas_distribution",
            "asset_name": "Pipeline",
            "visible": "1",
            "quality_metrics": "93%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a9741495aa33b377dd192b",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "6ecee39b-aa70-b7c1-b65c-775bdd567679",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 21,
            "geojson_dict": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [
                                    -122.27513379184529,
                                    37.87678470544788
                                ]
                            ]
                        },
                        "properties": {}
                    }
                ]
            },
            "asset_description": "Vegetationgrouphighpointreferstothehighestpointonatreeorgroupoftrees.Thegrouphighpointisannotatedintheformofapoint.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/tree_top.png",
            "classification_type": "points_geojson",
            "asset_category": "natural_elements",
            "asset_name": "VegetationGroupHighPoint",
            "visible": "1",
            "quality_metrics": "10cm",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a98eeb3a8c00930325db39",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "2d8531d8-8756-b8aa-fa47-274fc2cdcce6",
            "asset_image_1_url": "",
            "asset_time_complexity": "6",
            "processing_units_km": "1.5",
            "storefront_clicks": 10,
            "geojson_dict": "",
            "asset_description": "Pointsinthepointcloudwhichareunderneathground, asignificantdistanceawayfromtheotherpoints(5sigma).TheoutputisintheformofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "geojson_iframe": "",
            "asset_category": "point_cloud_classification",
            "asset_name": "Lowpointnoise(7)",
            "visible": "1",
            "quality_metrics": "99%",
            "processing_units_sqkm": "6",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55d2645f6c5e94d21f6c2281",
            "output_format": "classified_las_asprs",
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        },
        {
            "asset_id": "38e951ae-e1a9-3ac4-3bc0-87a2979068fa",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3.5",
            "storefront_clicks": 27,
            "geojson_dict": "https: //google-developers.appspot.com/maps/documentation/utils/geojson/",
            "asset_description": "Vegetationgroupcrownoutlinereferstotheoutlineofatree, shrubsoragroupoftrees.Thecrownoutlineisextractedintheformofapolygon.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/tree_crown.png",
            "classification_type": "polygon_geojson",
            "asset_category": "natural_elements",
            "asset_name": "VegetationGroupCrownOutline",
            "visible": "1",
            "quality_metrics": "88%",
            "processing_units_sqkm": "14",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a98eb43a8c00930325db38",
            "output_format": "geojson",
            "recommended_features": [
                "rgb",
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100"
            ]
        },
        {
            "asset_id": "04c28206-017b-61c4-4a60-88274407ef9f",
            "asset_image_1_url": "",
            "geojson_iframe": "undefined",
            "processing_units_km": "3",
            "storefront_clicks": 17,
            "geojson_dict": "",
            "asset_description": "Railclassificationreferstothepointsonthemetalrailsonarailwaytrack.TheoutputisinthefromofclassifiedLAS.",
            "annotated_image_url": "https: //s3-us-west-1.amazonaws.com/civil-maps-web/asset-pictures/classified_las.gif",
            "classification_type": "classified_las",
            "asset_category": "point_cloud_classification",
            "asset_name": "Rail(10)",
            "visible": "1",
            "quality_metrics": "97%",
            "processing_units_sqkm": "12",
            "asset_cad_url": "",
            "asset_image_2_url": "",
            "_id": "55a97e323a8c00930325db31",
            "output_format": "classified_las_asprs",
            "recommended_features": [
                "infrared"
            ],
            "survey_type": [
                "aerial_20",
                "aerial_100",
                "mobile",
                "terrestrial"
            ]
        }
    ]
}
