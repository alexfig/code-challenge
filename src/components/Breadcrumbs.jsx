import React from 'react';

export default class BreadCrumbs extends React.Component {
  renderSteps() {
    const {steps} = this.props;
    console.log(steps);
    const stepNames = Object.keys(steps);
    
    return stepNames.map((step, idx) => {
      const {complete, active} = steps[step];
      const lastCrumb = idx === stepNames.length-1;

      let stepAttribute = '';    
      if (complete) {
        stepAttribute = ' breadcrumb-step-complete';
      } else if (active) {
        stepAttribute = ' breadcrumb-step-active';
      }
      return (
        <div className={'breadcrumb-step-1-4' + stepAttribute} key={idx}>
        {step &&
          <div>
            <div className="breadcrumb-step-icon" href="#"></div>
            <div className="breadcrumb-step-info" href="#">{step}</div>
          </div>
        }
        {active &&
          <div className="breadcrumb-step-panel">
            {steps[step].panel}
          </div>
        }
        {!lastCrumb && 
          <div className="breadcrumb-step-line"></div>
        }
        </div>
      );
    })
    
  }
  render() {
    return (
      <div className="breadcrumb-steps">
        {this.renderSteps()}
      </div>
    );
  }
}
