import React from 'react';

export default class SurveyEstimates extends React.Component {
  render() {
    const { estimate, surveyVariables } = this.props;
    const area = estimate.area.toFixed(2);

    const { type, pointsPerM } = estimate.surveyType;
    const mobilizationTime = surveyVariables.mobilizationTime[type];
    const speed = surveyVariables.speed[type][pointsPerM];
    const costPerSqKm = surveyVariables.costPerSqKm.usa[type][pointsPerM];

    const surveyTime = mobilizationTime + Math.ceil(area/speed);
    const surveyCost = (estimate.dataSize * area * costPerSqKm).toFixed(2);

    return (
      <table className="estimates">
        <tbody>
          <tr className="estimates-row">
            <th>total map area:</th>
            <td>{area} <span className="estimates-units">sq. km</span></td>
          </tr>
          <tr className="estimates-row">
            <th>survey days:</th>
            <td>{surveyTime}<span className="estimates-units">days</span></td>
          </tr>
          <tr className="estimates-row">
            <th>estimate:</th>
            <td>${surveyCost}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}
