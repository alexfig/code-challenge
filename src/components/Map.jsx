import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { LINEAR, SQUARE } from 'constants/estimateConstants';

import 'leaflet';
import 'leaflet-draw';
import geodesy from 'leaflet-geodesy';

export default class Map extends React.Component {
  componentDidMount() {
    const osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    const osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors';
    const osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
    const map = this.map = new L.Map(ReactDOM.findDOMNode(this), {
      layers: [osm],
      center: new L.LatLng(-37.7772, 175.2756),
      zoom: 15,
      zoomControl: false
    });
    map.addControl(L.control.zoom({position: 'topright'}));
    const featureGroup = this.featureGroup = L.featureGroup().addTo(map);
    const drawControl = new L.Control.Draw({
      position: 'topright',
      draw: {
        polygon: {
          shapeOptions: {
            color: '#AC0000',
            weight: 5,
            opacity: 1
          }
        },
        polyline: {
          shapeOptions: {
            color: '#AC0000',
            weight: 5,
            opacity: 1
          }
        },
        rectangle: {
          shapeOptions: {
              color: '#AC0000',
              weight: 5,
              opacity: 1
          }
        },
        circle: false,
        marker: false
      }
    }).addTo(map);

    map.on('draw:created', (e)=>this.getArea(e));
  }

  componentWillUnmount() {
    this.map.off('draw:created', this.getArea);
    this.map = null;
  }

  render () {
    const { style } = this.props;
    return (
      <div className="map" style={style}>
        <a href="#" className="logo" />
      </div>
    );
  }

  getArea(e) {
    const featureGroup = this.featureGroup;
    featureGroup.clearLayers();
    featureGroup.addLayer(e.layer);

    if(e.layer.toGeoJSON().geometry.type === 'LineString') {
      const distance = getLength(e.layer.getLatLngs()) / 1000;
      e.layer.bindPopup((distance).toFixed(2) + ' km');
      this.props.updateLength(distance);
      this.props.updateArea(0);
    } else {
      const area = geodesy.area(e.layer) / 1000000;
      e.layer.bindPopup((area).toFixed(2) + ' km<sup>2</sup>');
      this.props.updateArea(area);
      this.props.updateLength(0);
    }
    this.props.updateSteps({
      'select area': {
        active: false,
        complete: true
      },
      'select features': {
        active: true
      }
    });
    e.layer.openPopup();
    
    function getLength(latLngs) {
      let distance = 0;
      latLngs.forEach((latLng, idx) => {
        if (idx !== latLngs.length-1) {
          distance += latLng.distanceTo(latLngs[idx+1]);
        }
      });
      return distance;
    }
  }
}
