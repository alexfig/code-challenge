import React from 'react';

export default class TitleInput extends React.Component {
  constructor() {
    super();
    this.state = {value: ''};
  }
  componentDidMount() {
    this.input.focus();
  }
  handleChange(e) {
    const title = e.target.value;
    this.setState({value: title});
    if(this.props.onChange) {
      this.props.onChange(title);
    }
  }
  render() {
    const value = this.state.value;
    return (
      <div className="title-estimate">
        <textarea
          value={value}
          placeholder="Enter Project Name"
          onChange={(e)=>this.handleChange(e)}
          ref={c => this.input = c}
        />
      </div>
    );
  }
}
