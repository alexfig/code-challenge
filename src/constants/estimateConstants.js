import { createConstants } from '../utils';

export default createConstants(
  'TITLE_UPDATE',
  'AREA_UPDATE',
  'LENGTH_UPDATE',
  'ASSET_ADD',
  'ASSET_REMOVE'
);
