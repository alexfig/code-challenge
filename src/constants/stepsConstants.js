import { createConstants } from '../utils';

export default createConstants(
  'INITIALIZE_STEPS',
  'STEPS_UPDATE'
);
