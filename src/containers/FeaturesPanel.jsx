import React from 'react';
import { connect } from 'react-redux';
import SearchInput from 'react-search-input';
import { addAsset, removeAsset } from 'actions/estimateActions';

import assetsJSON from '../assets/assets.js';
const allAssets = assetsJSON.report_spec;

const mapStateToProps = (state) => ({
  addedAssets: state.estimate.get('assets')
});
export class FeaturesPanel extends React.Component {
  clickHandler(asset, active) {
    if (active) {
      this.props.removeAsset(asset.asset_name);
    } else {
      this.props.addAsset({
        [asset.asset_name]: {
          creditPerKm: asset.processing_units_km,
          creditPerSqKm: asset.processing_units_sqkm,
          surveyType: asset.survey_type
        }
      });
    }
  }
  render() {
    const search = this.refs.search;
    const filters = ['asset_name'];
    const filteredAssets = search? allAssets.filter(search.filter(filters)) : allAssets;
    const { addedAssets } = this.props;
    return (
      <div className="feature-panel">
        <h5 className="feature-panel-header">Select Features to Map</h5>
        <div className="feature-searchbar">
          <select className="feature-select">
            <option value="">Industry</option>
            <option value="transportation">Transportation</option>
          </select>
        <SearchInput placeholder="Search..." ref='search' onChange={(term)=>this.searchUpdated(term)} className="feature-search"/>
          </div>
        <div style={{overflow:'auto', height: '400px'}}>
          <ul className="feature-list">
          {filteredAssets.map(asset => {
            const assetName = asset.asset_name;
            if (addedAssets.has(assetName)) {
              return (
                <li
                  className="feature-list-item feature-list-item-active"
                  onClick={()=>this.clickHandler(asset, true)}
                  key={assetName}
                >
                  <div className="feature-name">{assetName}</div>
                </li>
              );
            } else {
              return (
                <li
                  className="feature-list-item"
                  onClick={()=>this.clickHandler(asset, false)}
                  key={assetName}
                >
                  <div className="feature-name">{assetName}</div>
                </li>
              );
            }
          })}
          </ul>
        </div>
      </div>
    );
  }

  searchUpdated(term) {
    this.setState({searchTerm: term}); // needed to force re-render
  }
};

export default connect(mapStateToProps, { addAsset, removeAsset })(FeaturesPanel);
