import React from 'react';
import { connect } from 'react-redux';
import { updateTitle } from 'actions/estimateActions';
import { throttle } from 'lodash';

import TitleInput from 'components/TitleInput';
import BreadCrumbs from 'components/BreadCrumbs';
import Estimates from 'components/Estimates';

const mapStateToProps = (state) => ({
  estimate: state.estimate,
  survey:  state.survey
});

export class FooterContainer extends React.Component {
  static propTypes = {
    estimate: React.PropTypes.object.isRequired,
    steps : React.PropTypes.object.isRequired,
  }
  render() {
    const { steps } = this.props;
    const estimate = this.props.estimate.toJS();
    const survey = this.props.survey.toJS();
    const updateTitle = throttle(this.props.updateTitle, 1000, {leading:false});
    return (
      <footer className="footer">
        <div className="col-1-4">
          <TitleInput onChange={updateTitle}/>
        </div>
        <div className="col-1-2">
          <BreadCrumbs steps={steps} />
        </div>
        <div className="col-1-4">
          <Estimates estimate={estimate} surveyVariables={survey}/>
        </div>
      </footer>
    );
  }
}
export default connect(mapStateToProps, {updateTitle})(FooterContainer);
