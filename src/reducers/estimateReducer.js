import { createReducer } from '../utils';
import { TITLE_UPDATE, AREA_UPDATE, LENGTH_UPDATE, ASSET_ADD, ASSET_REMOVE, SQUARE } from 'constants/estimateConstants';
import { Map } from 'immutable'
const URBAN = 2;
const RURAL = 1;

const initialState = Map({
  dataSize: 10,
  area: 20,
  length: 0,
  assets: Map({}),
  landType: URBAN,
  bufferMultiplier: 2,
  gis: 5500,
  surveyType: Map({
    type: 'mobile',
    pointsPerM: 100
  })
});
export default createReducer(initialState, {
  [TITLE_UPDATE] : (state, title) => state.merge({title}),

  [AREA_UPDATE] : (state, area) => state.merge({area}),

  [LENGTH_UPDATE] : (state, length) => state.merge({length}),

  [ASSET_ADD] : (state, asset) => state.mergeIn(['assets'], asset),

  [ASSET_REMOVE] : (state, assetName) => state.deleteIn(['assets', assetName]),
});
