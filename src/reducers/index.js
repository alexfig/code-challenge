import { combineReducers }    from 'redux';
import { routeReducer }       from 'redux-simple-router';
import estimateReducer        from 'reducers/estimateReducer';
import surveyVariablesReducer from 'reducers/surveyVariablesReducer';
import stepsReducer from 'reducers/stepsReducer';

export default combineReducers({
  estimate: estimateReducer,
  survey: surveyVariablesReducer,
  steps: stepsReducer,
  routing: routeReducer
});
