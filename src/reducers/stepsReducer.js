import { createReducer } from '../utils';
import { INITIALIZE_STEPS, STEPS_UPDATE } from 'constants/stepsConstants';
import { OrderedMap, fromJS } from 'immutable'

const initialState = OrderedMap({});
export default createReducer(initialState, {
  [INITIALIZE_STEPS] : (state, steps) => OrderedMap(fromJS(steps)),

  [STEPS_UPDATE] : (state, steps) => state.mergeDeep(fromJS(steps)),

});
