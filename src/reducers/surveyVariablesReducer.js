import { createReducer } from '../utils';
import { Map } from 'immutable'

const initialState = Map({
  costPerSqKm: {
    usa: {
      arial: {
        20: 600,
        100: 1000
      },
      mobile: {
        100: 400,
        200: 600
      }
    }
  },
  mobilizationTime: {
    aerial: 20,
    mobile: 10
  },
  speed: {
    arial: {
      20: 30,
      100: 20
    },
    mobile: {
      100: 10,
      200: 10
    }
  }
});
export default createReducer(initialState, {

});
