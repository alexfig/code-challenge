import React from 'react';
import { Route, Redirect } from 'react-router';
import QuoteView from 'views/QuoteView';
import SpecView from 'views/SpecView';

export default (
  <Route>
    <Redirect from="/" to="/quote" />
    <Route name='quote' path='/quote' component={QuoteView} />
    <Route name='spec' path='/spec' component={SpecView} />
  </Route>
);
