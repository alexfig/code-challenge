import React from 'react';
import { connect } from 'react-redux';
import { updateArea, updateLength } from 'actions/estimateActions';
import { updateSteps, initializeSteps } from 'actions/stepsActions';

import Map from 'components/Map';
import Footer from 'containers/FooterContainer';
import panelArea from '../assets/panelArea.png'
import FeaturesPanel from 'containers/FeaturesPanel'
import sideDescription from '../assets/sideDescription.png'
import panelOptions from '../assets/panelOptions.png'

const mapStateToProps = (state) => ({
  steps: state.steps
});
const actions = {
  updateArea,
  updateLength,
  updateSteps,
  initializeSteps
};
export class QuoteView extends React.Component {
  componentWillMount() {
    const areaPanel = <img src={panelArea} />;
    const featuresPanel = <FeaturesPanel />;
    const steps = {
      'select area': {
        active: true,
        panel: areaPanel
      },
      'select features': {
        panel: featuresPanel
      },
      '': {
      },
      'Get Quote': {
      },
    };
    this.props.initializeSteps(steps);
  }
  render() {
    const steps = this.props.steps.toJS();
    const { updateArea, updateLength, updateSteps } = this.props;
    const style = {
      position: 'absolute',
      top: 0,
      bottom: '80px',
      width:'100%'
    };
    return (
      <div className="app">
        <Map updateArea={updateArea} updateLength={updateLength} updateSteps={updateSteps} style={style}/>
        <Footer steps={steps} />
      </div>
    );
  }
}
export default connect(mapStateToProps, actions )(QuoteView);

