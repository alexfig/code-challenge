import React from 'react';
import { connect } from 'react-redux';
import { updateArea, updateLength } from 'actions/estimateActions';
import { updateSteps, initializeSteps } from 'actions/stepsActions';

import Map from 'components/Map';
import Footer from 'containers/FooterContainer';
import panelArea from '../assets/panelArea.png'
import sideDescription from '../assets/sideDescription.png'
import panelOptions from '../assets/panelOptions.png'
import FeaturesPanel from 'containers/FeaturesPanel'

const mapStateToProps = (state) => ({
  title: state.estimate.get('title')
});
const actions = {
  updateArea,
  updateLength,
  updateSteps,
  initializeSteps
};
export class SpecView extends React.Component {
  render() {
    const { updateArea, updateLength } = this.props;
    const areaPanel = <img src={panelArea} />;
    const featuresPanel = <FeaturesPanel />;
    const optionsPanel = <img src={panelOptions} />;
    const steps = {
      'select area': {
        complete: true,
        panel: areaPanel
      },
      'select features': {
        complete: true,
        panel: featuresPanel
      },
      'select options': {
        complete: true,
        panel: optionsPanel
      },
      'generate specs': {
        active: true
      },
    };
    const mapStyle = {
      position: 'absolute',
      top: 0,
      bottom: '80px',
      width:'50%'
    };
    const specStyle = {
      position: 'absolute',
      top: 0,
      left: '50%',
      bottom: '80px',
      width:'50%',
      backgroundColor: '#474c57'
    };
    const headerStyle = {
      width: '100%',
      height: '100px',
      backgroundColor: 'black',
      color: '#F5A623'
    };
    return (
      <div className="app">
        <Map updateArea={updateArea} updateLength={updateLength} updateSteps={updateSteps} style={mapStyle}/>
        <div style={specStyle}>
          <header style={headerStyle}>
            <h2 className="spec-header">{this.props.title || 'College Ave. Bike Path'}</h2>
          </header>
          <div style={{height: '660px', width: '490px', backgroundColor:'white', margin: '30px'}}></div>
        </div>
        <Footer steps={steps} />
      </div>
    );
  }
}

export default connect(mapStateToProps, actions )(SpecView);
